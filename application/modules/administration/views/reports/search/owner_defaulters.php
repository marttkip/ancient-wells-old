        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
            	<h2 class="panel-title">Search</h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
			<?php
            echo form_open("search-defaulters-owners", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Property</label>
                        
                        <div class="col-lg-8">
                           <select id='property_id' name='property_id' class='form-control custom-select '>
                              <option value=''>None - Please Select a property</option>
                              <?php echo $property_list;?>
                            </select>
                        </div>
                    </div>

                </div>
                
                <div class="col-md-3">
                    
                    
                </div>
                
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Branch </label>
                        
                        <div class="col-lg-6">
                            <select id='branch_code' name='branch_code' class='form-control custom-select'>
                            	<option value="">---Select branch---</option>
                                <?php
									echo $branches_list;
								?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row " >
                 <div class="form-group ">
                        <div class="center-align">
                            <div >
                                <button type="submit" class="btn btn-info">Search Debtors</button>
                            </div>
                        </div>
                    </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
          </div>
		</section>