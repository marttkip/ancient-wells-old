<?php
	$result = '';
	$action_point = '';
	//if users exist display them
	if ($query->num_rows() > 0)
	{
		$count = $page;
		
		$result .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Document Number</th>
					<th>Property Name</th>
					<th colspan="1">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($query->result() as $row)
		{
			$record_id = $row->record_id;
			$property_id = $row->property_id;
			$property_name = $row->property_name;
			$document_number = $row->document_number;	
			
			$count++;
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$document_number.'</td>
					<td>'.$property_name.'</td>
					<td><a href="'.site_url().'send-water-invoices/'.$record_id.'"  class="btn btn-sm btn-warning" ><i class="fa fa-inbox"></i> Send reminders</a></td>
					<td><a href="'.site_url().'print-water-readings/'.$document_number.'" target="_blank" class="btn btn-sm btn-danger" ><i class="fa fa-print"></i> Print Documents</a></td>
	
				</tr> 
			';
	
			// $action_point = '<div class="pull-right"><a class="btn btn-success btn-sm" href="'.site_url().'messaging/send-messages" style="margin-top: -5px;" onclick="return confirm(\'Do you want to send the messages ?\');" title="Send Message">Send Unsent Messages</a></div>';
		}
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result .= "There are no messages";
	}
	
	$result2 = '';
	//if users exist display them
	if ($property_invoices->num_rows() > 0)
	{
		$count = 0;
		
		$result2 .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Invoice Date</th>
					<th>Property</th>
					<th>Type</th>
					<th>Invoice Number</th>
					<th>Invoice Amount</th>
					<th>Invoice Units</th>
					<th>Created</th>
					<th colspan="1">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($property_invoices->result() as $row)
		{
			$property_invoice_id = $row->property_invoice_id;
			$invoice_type_name = $row->invoice_type_name;
			$property_name = $row->property_name;
			$property_invoice_number = $row->property_invoice_number;
			$property_invoice_date = date('jS M Y',strtotime($row->property_invoice_date));
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_units = $row->property_invoice_units;
			$created = date('jS M Y H:i a',strtotime($row->created));
			
			$count++;
			$result2 .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$property_invoice_date.'</td>
					<td>'.$property_name.'</td>
					<td>'.$invoice_type_name.'</td>
					<td>'.$property_invoice_number.'</td>
					<td>'.number_format($property_invoice_amount, 2).'</td>
					<td>'.$property_invoice_units.'</td>
					<td>'.$created.'</td>
					<td><a href="'.site_url().'delete-property-invoices/'.$property_invoice_id.'"  class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this invoice\')"><i class="fa fa-trash"></i> Delete</a></td>
	
				</tr> 
			';
		}
		
		$result2 .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result2 .= "There are no invoices";
	}
?>

<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title">Readings</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
            	<?php
					$success = $this->session->userdata('success_message');
				
					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
				
					$error = $this->session->userdata('error_message');
				
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}
				?>
				
				<?php
					if(isset($import_response))
					{
						if(!empty($import_response))
						{
							echo $import_response;
						}
					}
					
					if(isset($import_response_error))
					{
						if(!empty($import_response_error))
						{
							echo '<div class="center-align alert alert-danger">'.$import_response_error.'</div>';
						}
					}
				?>
                <div class="tabs">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a class="text-center" data-toggle="tab" href="#property"><i class="fa fa-building"></i> Property Invoices</a>
                        </li>
                        <li>
                            <a class="text-center" data-toggle="tab" href="#tentant"><i class="fa fa-users"></i> Tenant Readings</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tentant">
                            <section class="panel">
                                <header class="panel-heading">						
                                    <h2 class="panel-title"><?php echo $title;?> <?php echo $action_point;?></h2>
                                </header>
                                <div class="panel-body">
                                    <?php echo form_open_multipart("import-water-readings", array("class" => "form-horizontal","role" => "form"));?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <ul>
                                                    <li>Download the import template <a href="<?php echo site_url().'import-water-readings-template';?>">here.</a></li>
                                                    
                                                    <li>Save your file as a <strong>csv</strong> file before importing</li>
                                                    <li>Select the Property </li>
                                                    <li>Set the invoice date (Note : This is the date that will appear as the invoiced date) </li>
                                                    <li>Set the total total units consumed for the month</li>
                
                                                </ul>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Invoice: </label>
                                                    <div class="col-lg-7">
                                                        <select class="form-control" name="property_invoice_id">
                                                            <option value=""> --Select an invoice-- </option>
                                                            <?php
                                                            if($property_invoices->num_rows() > 0)
                                                            {
                                                                foreach ($property_invoices->result() as $row) {
                                                                    # code...
																	$invoice_type_name = $row->invoice_type_name;
																	$property_name = $row->property_name;
                                                                    $property_invoice_id = $row->property_invoice_id;
																	$property_invoice_number = $row->property_invoice_number;
																	$property_invoice_amount = number_format($row->property_invoice_amount, 2);
																	$property_invoice_units = number_format($row->property_invoice_units, 0);
                                                                    ?>
                                                                    <option value="<?php echo $property_invoice_id;?>"><?php echo $property_name;?> - <?php echo $invoice_type_name;?> - <?php echo $property_invoice_number;?> - <?php echo $property_invoice_amount;?> - <?php echo $property_invoice_units;?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--<div class="form-group">
                                                    <label class="col-lg-5 control-label">Type: </label>
                                                    <div class="col-lg-7">
                                                        <select class="form-control" name="invoice_type_id">
                                                            <option value=""> --Select a type-- </option>
                                                            <?php
                                                            if($invoice_types->num_rows() > 0)
                                                            {
                                                                foreach ($invoice_types->result() as $key) {
                                                                    # code...
                                                                    $invoice_type_id = $key->invoice_type_id;
                                                                    $invoice_type_name = $key->invoice_type_name;
                                                                    ?>
                                                                    <option value="<?php echo $invoice_type_id;?>"><?php echo $invoice_type_name;?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>-->
                                                <!-- <div class="fileUpload btn btn-primary"> -->
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Import Rental Arreas List</label>
                                                    <div class="col-lg-7">
                                                        <input type="file" class="upload" onChange="this.form.submit();" name="import_csv" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <?php echo $result;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <?php if(isset($links)){echo $links;}?>
                                </div>
                            </section>
                        </div>
                        
                        <!-- Property Uploads -->
                        <div class="tab-pane active" id="property">
                            <section class="panel">
                                <header class="panel-heading">						
                                    <h2 class="panel-title">Property</h2>
                                </header>
                                <div class="panel-body">
                                    <?php echo form_open_multipart("add-property-readings", array("class" => "form-horizontal","role" => "form"));?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Type: </label>
                                                    <div class="col-lg-7">
                                                        <select class="form-control" name="invoice_type_id">
                                                            <option value=""> --Select a type-- </option>
                                                            <?php
                                                            if($invoice_types->num_rows() > 0)
                                                            {
                                                                foreach ($invoice_types->result() as $key) {
                                                                    # code...
                                                                    $invoice_type_id = $key->invoice_type_id;
                                                                    $invoice_type_name = $key->invoice_type_name;
                                                                    ?>
                                                                    <option value="<?php echo $invoice_type_id;?>"><?php echo $invoice_type_name;?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Property Name: </label>
                                                    <div class="col-lg-7">
                                                        <select class="form-control" name="property_id">
                                                            <option value=""> --Select a property-- </option>
                                                            <?php
                                                            if($properties->num_rows() > 0)
                                                            {
                                                                foreach ($properties->result() as $key) {
                                                                    # code...
                                                                    $property_id = $key->property_id;
                                                                    $property_name = $key->property_name;
                                                                    ?>
                                                                    <option value="<?php echo $property_id;?>"><?php echo $property_name;?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Invoice Date: </label>
                                                    
                                                    <div class="col-lg-7">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="property_invoice_date" placeholder="Invoice Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Invoice Number: </label>
                                                    
                                                    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="property_invoice_number" placeholder="Invoice Number" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Total Units Consumed: </label>
                                                    
                                                    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="property_invoice_units" placeholder="Units consumed" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-5 control-label">Total Invoice Amount: </label>
                                                    
                                                    <div class="col-lg-7">
                                                        <input type="text" class="form-control" name="property_invoice_amount" placeholder="Invoice Amount" value="">
                                                    </div>
                                                </div>
                                                <!-- <div class="fileUpload btn btn-primary"> -->
                                                <div class="form-group center-align">
                                                    <input type="submit" class="btn btn-primary" value="Add Invoice" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <?php echo $result2;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                                                    
                    </div>
                </div>
                                                
            </div>
        </div>
    </div>
</section>