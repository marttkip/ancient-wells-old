<?php
//personnel data
$lease_start_date = set_value('lease_start_date');
$lease_end_date = set_value('lease_end_date');
$tenant_id = set_value('tenant_id');

$rent_amount = set_value('rent_amount');
$rent_amount_period = set_value('rent_amount_period');
$rent_deposit = set_value('rent_deposit');
$water_deposit = set_value('water_deposit');
$electricity_deposit = set_value('electricity_deposit');

$garbage_charges = set_value('garbage_charges');
$security_charges = set_value('security_charges');
$garbage_charges_period = set_value('garbage_charges_period');
$security_charges_period = set_value('security_charges_period');
$water_charges = set_value('water_charges');
$water_charges_period = set_value('water_charges_period');

$rent_arrears = set_value('rent_arrears');
$water_arrears = set_value('water_arrears');
$electricity_arrears = set_value('electricity_arrears');
$garbage_arrears = set_value('garbage_arrears');
$security_arrears = set_value('security_arrears');


?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>setup/property" class="btn btn-info pull-right">Back to Properties</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-5 control-label">Lease Start Date: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="lease_start_date" placeholder="" value="<?php echo $lease_start_date;?>">
            </div>
        </div>
        
        <div class="form-group">
          <label class="col-lg-5 control-label">Lease End Date: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="lease_end_date" placeholder="" value="<?php echo $lease_end_date;?>">
            </div>
        </div>

        <div class="form-group">
          <label class="col-lg-5 control-label">Tenant Data: </label>
            
           <div class="col-lg-7">
                <select class="form-control" name="rent_amount_period">
                    <option value="">--Select Tenant Data--</option>
                    <?php
                        if($products->num_rows() > 0)
                        {
                            $veh = $products->result();
                            
                            foreach($veh as $res)
                            {
                                $product_id = $res->tenant_id;
                                $product_name = $res->tenant_name;
                                
                                if($product_id == $product_name)
                                {
                                    echo '<option value="'.$product_id.'" selected>'.$product_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$product_id.'">'.$product_name.'</option>';
                                }
                            }
                        }
                    ?>
                    
                   
                </select>
             </div>
        </div>

         <div class="form-group">
          <label class="col-lg-5 control-label">Rent Amount: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="rent_amount" placeholder="" value="<?php echo $rent_amount;?>">
            </div>
        </div>

        <div class="form-group">
          <label class="col-lg-5 control-label">Rent Amount Period: </label>
            
           <div class="col-lg-7">
                <select class="form-control" name="rent_amount_period">
                    <option value="">--Select Period--</option>
                    <option value="1"> Monthly </option>
                    <option value="2"> Semi-Annually </option>
                    <option value="3"> Annually </option>
                    
                   
                </select>
             </div>
        </div>

         <div class="form-group">
          <label class="col-lg-5 control-label">Rent Deposit Amount: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="rent_deposit" placeholder="" value="<?php echo $rent_deposit;?>">
            </div>
        </div>
         <div class="form-group">
          <label class="col-lg-5 control-label">Water Deposit Amount: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="water_deposit" placeholder="" value="<?php echo $water_deposit;?>">
            </div>
        </div>
         <div class="form-group">
          <label class="col-lg-5 control-label">Electricity Deposit Amount: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="electricity_deposit" placeholder="" value="<?php echo $electricity_deposit;?>">
            </div>
        </div>
          <div class="form-group">
            <label class="col-lg-5 control-label">Garbage Charges: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="garbage_charges" placeholder="" value="<?php echo $garbage_charges;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Garbage Charges Period: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="garbage_charges_period">
                    <option value="">--Select Period--</option>
                    <option value="1"> Monthly </option>
                    <option value="2"> Semi-Annually </option>
                    <option value="3"> Annually </option>
                    
                   
                </select>
             </div>
        </div>
        
     
         
        
    </div>
    
    <div class="col-md-6">
        
       
        
      
        
       <div class="form-group">
            <label class="col-lg-5 control-label">Security Charges: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="security_charges" placeholder="" value="<?php echo $security_charges;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Security Charges Period: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="security_charges_period">
                    <option value="">--Select Period--</option>
                    <option value="1"> Monthly </option>
                    <option value="2"> Semi-Annually </option>
                    <option value="3"> Annually </option>
                    
                   
                </select>
             </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Water Charges: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="water_charges" placeholder="" value="<?php echo $water_charges;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Water Charges Period: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="water_charges_period">
                    <option value="">--Select Period--</option>
                    <option value="1"> Monthly </option>
                    <option value="2"> Semi-Annually </option>
                    <option value="3"> Annually </option>
                    <option value="4"> Unit Based </option>
                    
                   
                </select>
             </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Rent Arrears: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="rent_arrears" placeholder="" value="<?php echo $rent_arrears;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Water Arrears: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="water_arrears" placeholder="" value="<?php echo $water_arrears;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Garbage Arreas: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="garbage_arrears" placeholder="" value="<?php echo $garbage_arrears;?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Security Arrears: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="security_arrears" placeholder="" value="<?php echo $security_arrears;?>">
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Electricity Arrears: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="electricity_arrears" placeholder="" value="<?php echo $electricity_arrears;?>">
            </div>
        </div>
    
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Lease Details
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>