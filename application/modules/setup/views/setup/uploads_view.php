<?php
	$result = '';
	
	//if users exist display them
	if ($query->num_rows() > 0)
	{
		$count = $page;
		
		$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Document Name</th>
						<th>Date Created</th>
						<th>Created By</th>
						<th>Document Link</th>
					</tr>
				</thead>
				
				<tbody>
			';
		
					//get all administrators
					// $personnel_query = $this->personnel_model->get_all_personnel();
					// var_dump($query->num_rows()); die();
					foreach ($query->result() as $row)
					{
						$upload_id = $row->upload_id;
						$upload_document_name = $row->upload_document_name;
						$document_name = $row->document_name;
						$created = $row->created;
						$created_by = $row->created_by;

						$created_by = $this->setup_model->get_personnel_name($created_by);

						$created = date('jS M Y',strtotime($created));
										
						$status = '<span class="label label-info"> Active </span>';
						$count++;
						$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$document_name.'</td>
								<td>'.$created.'</td>
								<td>'.$created_by.'</td>
								<td><a href="'.site_url().'assets/uploads/'.$upload_document_name.'" class="btn btn-sm btn-warning" title="Open '.$document_name.'" target="_blank"><i class="fa fa-eye"></i> Open Document</a></td>
								
							</tr> 
						';
						// }
					}
		
			$result .= 
			'
						  </tbody>
						</table>
			';
	}
	
	else
	{
		$result .= "There are no projects created";
	}
	$result .= '';
	
?>

<div class="row">

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title"><?php echo $title;?></h2>
			
			 <a href="<?php echo base_url()?>setup/tenant" class="btn btn-info btn-sm pull-right fa fa-arrow-left" style="margin-right:5px;"> Back to Tenants</a>
			 <a data-toggle="modal" data-target="#upload_project" class="btn btn-warning btn-sm pull-right" style="margin-right:5px;">Upload Document</a>

			 <div class="modal fade" id="upload_project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Upload Projects</h4>
						</div>
						<div class="modal-body">  

					        <!-- Widget content -->
							<div class="panel-body">
						        <div class="padd">
							        <div class="row">
								        <div class="col-md-12">
									           <?php echo form_open_multipart(base_url().'tenants-uploads/'.$tenant_id, array("class" => "form-horizontal", "role" => "form"));?>
						                            <div class="row">
										                <div class="col-sm-12">
										                    <div class="form-group">
										                        <label class="col-lg-4 control-label">Document Name</label>
										                        <div class="col-lg-8">
										                        	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
										                            <input type="text" class="form-control" name="document_name" placeholder="Document Name" value="" required>
										                        </div>
										                    </div>
										                </div>
										                <div class="col-sm-12"> 
											                <div class="form-group">
												                <label class="col-lg-4 control-label">Attachments</label>
												                <div class="col-lg-8">
												                    <input type="file" name="post_image">
												                </div>
												            </div>
										                </div>
					                            	</div>
										            <br>
										            <div class="row center-align">
										             	<div class="form-actions center-align">
											                <button class="submit btn btn-primary btn-sm" type="submit">
											                    Upload Document
											                </button>
											            </div>
										            </div>
									          <?php echo form_close();?>
										</div>
									</div>
					            </div>
					        </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      		</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="panel-body">
			<div class="row" style="margin-top:10px;">
    			<div class="col-lg-12 col-sm-12 col-md-12">
    				<div class="padd">
					<!-- end add request event -->
					  <?php
						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							$this->session->unset_userdata('error_message');
						}
						?>
					
						<?php echo $result;?>
					</div>
				</div>
			</div>

			<div class="panel-foot">
		        
				<?php if(isset($links)){echo $links;}?>
		    
		        <div class="clearfix"></div> 
		    
		    </div>
		</div>
	</section>

</div>