<?php
	$result = '';
	
	//if users exist display them
	if ($query->num_rows() > 0)
	{
		$count = $page;
		
		$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Complaint</th>
						<th>Date Created</th>
						<th>Created By</th>
						<th></th>
					</tr>
				</thead>
				
				<tbody>
			';
		
					//get all administrators
					// $personnel_query = $this->personnel_model->get_all_personnel();
					// var_dump($query->num_rows()); die();
					foreach ($query->result() as $row)
					{
						$complaint_id = $row->complaint_id;
						$complaint = $row->complaint;
						$created = $row->created;
						$created_by = $row->created_by;

						$created_by = $this->setup_model->get_personnel_name($created_by);

						$created = date('jS M Y',strtotime($created));
										
						$status = '<span class="label label-info"> Active </span>';
						$count++;
						$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$complaint.'</td>
								<td>'.$created.'</td>
								<td>'.$created_by.'</td>
								<td>'.$status.'</td>
								
							</tr> 
						';
						// }
					}
		
			$result .= 
			'
						  </tbody>
						</table>
			';
	}
	
	else
	{
		$result .= "There are no projects created";
	}
	$result .= '';
	
?>

<div class="row">

	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title"><?php echo $title;?></h2>
			
			 <a href="<?php echo base_url()?>setup/tenant" class="btn btn-info btn-sm pull-right fa fa-arrow-left" style="margin-right:5px;"> Back to Tenants</a>
			 <a data-toggle="modal" data-target="#upload_project" class="btn btn-warning btn-sm pull-right" style="margin-right:5px;">Add Comlaint</a>

			 <div class="modal fade" id="upload_project" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Add Complaint</h4>
						</div>
						<div class="modal-body">  

					        <!-- Widget content -->
							<div class="panel-body">
						        <div class="padd">
							        <div class="row">
								        <div class="col-md-12">
									           <?php echo form_open_multipart(base_url().'tenants-complaints/'.$tenant_id, array("class" => "form-horizontal", "role" => "form"));?>
						                            <div class="row">
										                <div class="col-sm-12">
										                    <div class="form-group">
										                        <label class="col-lg-3 control-label">Complaint</label>
										                        <div class="col-lg-9">
										                        	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
										                        	<textarea class="form-control" name="complaint" placeholder="Complaint"></textarea>
										                        </div>
										                    </div>
										                </div>
					                            	</div>
										            <br>
										            <div class="row center-align">
										             	<div class="form-actions center-align">
											                <button class="submit btn btn-primary btn-sm" type="submit">
											                    Add Complaint
											                </button>
											            </div>
										            </div>
									          <?php echo form_close();?>
										</div>
									</div>
					            </div>
					        </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      		</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="panel-body">
			<div class="row" style="margin-top:10px;">
    			<div class="col-lg-12 col-sm-12 col-md-12">
    				<div class="padd">
					<!-- end add request event -->
					  <?php
						$error = $this->session->userdata('error_message');
						$success = $this->session->userdata('success_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							$this->session->unset_userdata('error_message');
						}
						?>
					
						<?php echo $result;?>
					</div>
				</div>
			</div>

			<div class="panel-foot">
		        
				<?php if(isset($links)){echo $links;}?>
		    
		        <div class="clearfix"></div> 
		    
		    </div>
		</div>
	</section>

</div>