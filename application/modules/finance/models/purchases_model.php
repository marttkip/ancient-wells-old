<?php

class Purchases_model extends CI_Model
{
  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  public function get_account_payments_transactions($table, $where, $config, $page, $order, $order_method)
  {
    //retrieve all accounts
    $this->db->from($table);
    $this->db->select('finance_purchase.*,property.property_name,creditor.creditor_name,account.account_name');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('creditor', 'creditor.creditor_id = finance_purchase.creditor_id','left');
    $this->db->join('account', 'account.account_id = finance_purchase.account_to_id','left');
    $this->db->join('property', 'property.property_id = finance_purchase.property_id','left');
    $query = $this->db->get('', $config, $page);

    return $query;
  }

  public function get_creditor()
  {
    //retrieve all users
    $this->db->from('creditor');
    $this->db->select('*');
    $this->db->where('creditor_id >0 ');
    $query = $this->db->get();

    return $query;
  }




  public function add_payment_amount()
  {

    $document_number_two = $this->create_purchases_number();
    $account = array(
          'account_to_id'=>$this->input->post('account_to_id'),
          'property_id'=>$this->input->post('property_id'),
          'finance_purchase_amount'=>$this->input->post('transacted_amount'),
          'finance_purchase_description'=>$this->input->post('description'),
          'creditor_id'=>$this->input->post('creditor_id'),
          'transaction_number'=>$this->input->post('transaction_number'),
          'transaction_date'=>$this->input->post('transaction_date'),
          'created_by'=>$this->session->userdata('personnel_id'),
          'document_number'=>$document_number_two,
          'created'=>date('Y-m-d H:i:s'),
          'last_modified'=>date('Y-m-d H:i:s')
          );
    // var_dump($account); die();
    if($this->db->insert('finance_purchase',$account))
    {
      $finance_purchase_id = $this->db->insert_id();

      // $document_number = $this->create_purchases_payment();
      // $account = array(
      //       'account_from_id'=>$this->input->post('account_from_id'),
      //       'finance_purchase_id'=>$finance_purchase_id,
      //       'amount_paid'=>$this->input->post('transacted_amount'),
      //       'transaction_date'=>$this->input->post('transaction_date'),
      //       'transaction_number'=>$this->input->post('reference_number'),
      //       'transaction_date'=>$this->input->post('transaction_date'),
      //       'created_by'=>$this->session->userdata('personnel_id'),
      //       'created'=>date('Y-m-d'),
      //       'document_number'=>$document_number
      //       );
      // if($this->db->insert('finance_purchase_payment',$account))
      // {
          return TRUE;
      // }

    }
    else
    {
      return FALSE;
    }
  }
  public function payaninvoice($finance_purchase_id)
  {
    $document_number = $this->create_purchases_payment();
    $account = array(
          'account_from_id'=>$this->input->post('account_from_id'),
          'finance_purchase_id'=>$finance_purchase_id,
          'amount_paid'=>$this->input->post('amount_paid'),
          'transaction_date'=>$this->input->post('payment_date'),
          'transaction_number'=>$this->input->post('reference_number'),
          'created_by'=>$this->session->userdata('personnel_id'),
          'created'=>date('Y-m-d H:i:s'),
          'last_modified'=>date('Y-m-d H:i:s'),
          'document_number'=>$document_number
          );
    if($this->db->insert('finance_purchase_payment',$account))
    {
        return TRUE;
    }
    else {
      return FALSE;
    }
  }
  public function get_all_purchase_invoices()
  {
    //retrieve all users
    $this->db->from('finance_purchase,account');
    $this->db->select('*');
    $this->db->where('finance_purchase_id > 0 AND finance_purchase.account_to_id = account.account_id');
    $query = $this->db->get();

    return $query;
  }

  public function get_amount_paid($finance_purchase_id)
  {
    $this->db->from('finance_purchase_payment');
    $this->db->select('SUM(amount_paid) AS total_amount');
    $this->db->where('finance_purchase_id = '.$finance_purchase_id.' AND finance_purchase_payment_status = 1');
    $query = $this->db->get();
    $total_amount = 0;

    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        // code...
        $total_amount = $value->total_amount;
      }
    }
    return $total_amount;
  }
  function create_purchases_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('finance_purchase');
		$this->db->where("finance_purchase_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}


  function create_purchases_payment()
  {
    //select product code
    $preffix = "HA-RT-";
    $this->db->from('finance_purchase_payment');
    $this->db->where("finance_purchase_payment_id > 0");
    $this->db->select('MAX(document_number) AS number');
    $query = $this->db->get();//echo $query->num_rows();

    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;

      $number++;//go to the next number
    }
    else{//start generating receipt numbers
      $number = 1;
    }

    return $number;
  }

  public function get_child_accounts($parent_account_name,$type=null)
  {
      $this->db->from('account');
      $this->db->select('*');
      $this->db->where('account_name = "'.$parent_account_name.'"');
      $query = $this->db->get();

      if($query->num_rows() > 0)
      {
        foreach ($query->result() as $key => $value) {
          # code...
          $account_id = $value->account_id;
        }
        $values ='';
        if(!empty($type))
        {
          $values = ' AND account_id <> '.$type;
        }
        //retrieve all users
        $this->db->from('account');
        $this->db->select('*');
        $this->db->where('parent_account = '.$account_id.$values);
        $query = $this->db->get();

        return $query;


      }
      else
      {
        return FALSE;
      }

  }

}
?>
