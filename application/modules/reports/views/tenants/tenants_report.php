<?php


$tenant_id = $this->session->userdata('tenants_id_searched');
if(empty($tenant_id))
{


$tenant_where = 'tenant_id > 0' ;
$tenant_table = 'tenants';
$tenant_order = 'tenant_id';

$tenant_query = $this->reports_model->get_list_items($tenant_table, $tenant_where, $tenant_order);
$rs8 = $tenant_query->result();
$tenant_list = '';
foreach ($rs8 as $tenant_rs) :
  $tenant_id = $tenant_rs->tenant_id;
  $tenant_name = $tenant_rs->tenant_name;
  $tenant_number = $tenant_rs->tenant_number;

    $tenant_list .="<option value='".$tenant_id."'>".$tenant_number." ".$tenant_name."</option>";

endforeach;

?>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title;?></h3>

          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="box-body">
            <!-- select a tenant  -->
            <div class="row" style="margin-bottom:20px;">
              <?php echo form_open("search-tenants-report", array("class" => "form-horizontal", "role" => "form"));?>
                  <div class="row">
                    <div class="col-md-12">
                          <div class="col-md-8">
                                <div class="form-group center-align">
                                  <label class="col-md-4 control-label">Tenant Name: </label>

                                  <div class="col-md-8">
                                    <select id='tenant_id' name='tenant_id' class='form-control select2'>
                                        <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
                                          <option value=''>None - Please Select a tenant</option>
                                          <?php echo $tenant_list;?>
                                        </select>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                          <div class="form-actions center-align">
                              <button class="submit btn btn-primary btn-sm" type="submit">
                                  Search tenant
                              </button>
                          </div>
                        </div>

                    </div>
                  </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
  </div>
<?php

}
else {
  $tenant_id = $this->session->userdata('tenants_id_searched');
  $tenant_where = 'tenant_id  ='.$tenant_id ;
  $tenant_table = 'tenants';
  $tenant_order = 'tenant_id';

  $tenant_query = $this->reports_model->get_list_items($tenant_table, $tenant_where, $tenant_order);
  $rs8 = $tenant_query->result();
  foreach ($rs8 as $tenant_rs) :
    $tenant_id = $tenant_rs->tenant_id;
    $tenant_name = $tenant_rs->tenant_name;
    $tenant_number = $tenant_rs->tenant_number;
  endforeach;

  ?>

  <?php



  $where = 'property.property_id = personnel_properties.property_id AND property_owners.property_owner_id = property.property_owner_id';
  $table = 'property,personnel_properties,property_owners';
  $property_query = $this->dashboard_model->get_content($table, $where,$select='*',$group_by=NULL,$limit=NULL);
  $rs8 = $property_query->result();
  $total_properties = $property_query->num_rows();
  $property_list = '';
  $assigned_properties = '';
  if($total_properties > 0)
  {
   $x = 0;
   foreach ($rs8 as $property_rs) :
     $property_id = $property_rs->property_id;
     $property_name = $property_rs->property_name;
     $property_owner_name = $property_rs->property_owner_name;
     $x++;

      $property_list .= ' AND rental_unit.property_id ='.$property_id;
      if($x < $total_properties)
      {
        $property_list .= ',';
      }

      $received_where = 'rental_unit_status = 1 AND property_id  ='.$property_id;
      $received_table = 'rental_unit';
      $total_property_units = $this->dashboard_model->count_items($received_table, $received_where);
      $assigned_properties .= ' <li>
                                 <a class="users-list-name" href="#">'.$property_name.'</a>
                                 <span class="users-list-date">Owner : '.$property_owner_name.'</span>
                                 <span class="users-list-date">'.$total_property_units.' Units</span>
                                 <div class="progress sm">
                                   <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                 </div>
                               </li>';

   endforeach;
  }



  $received_where = 'rental_unit_status = 1 '.$property_list;
  $received_table = 'rental_unit';

  $total_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


  $received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status < 4 '.$property_list;
  $received_table = 'rental_unit,tenant_unit,leases';

  $occupied_rental_units = $active_leases = $this->dashboard_model->count_items($received_table, $received_where);



  $received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id NOT IN (SELECT leases.tenant_unit_id FROM leases WHERE leases.lease_status < 4) '.$property_list;
  $received_table = 'rental_unit,tenant_unit';

  $unoccupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);

  // percentage


  if($total_rental_units > 0 AND $unoccupied_rental_units > 0)
  {
  $occupied_percentage = ($occupied_rental_units / $total_rental_units) * 100;
  }
  else
  {
   $occupied_percentage = 0;
  }


  $revenue_percentage = 1;//$total_payments/$total_invoices * 100;



  // $chart_array = array();

  $chartExpenses = '';
  $chartIncomes = '';
  $total_labels= '';

  for ($i = 6; $i >= 0; $i--) {
   // code...
   $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
   $months_explode = explode('-', $months);
   $year = $months_explode[0];
   $month = $months_explode[1];
   $last_visit = date('M Y',strtotime($months));

   $total_amount = $this->reports_model->get_creditor_service_amounts(NULL,$month,$year);
   $total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type(NULL,$month,$year);

   $total_labels .=  "'".$last_visit."',";

   if(empty($total_tenant_inflow_amt))
   {
     $total_tenant_inflow_amt = 0;
   }

   if(empty($total_amount))
   {
     $total_amount = 0;
   }

   $chartIncomes .= $total_tenant_inflow_amt.',';
   $chartExpenses .= $total_amount.',';
  }

  // var_dump($chartExpenses); die();


  $received_where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_deleted = 0 AND MONTH(leases.lease_end_date) ="'.date('m').'" AND YEAR(leases.lease_end_date) ="'.date('Y').'"  ';
  $received_where .= $property_list;
  $received_table = 'leases,rental_unit,tenant_unit,tenants,property';
  $total_due_leases = $this->dashboard_model->count_items($received_table, $received_where);

  $leases_result = '';

  $received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.date('m').'"
  AND lease_invoice.invoice_year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 ';
  $received_select = 'sum(invoice.invoice_amount) AS number';
  $received_table = 'invoice,lease_invoice';
  $total_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


  $datestring=''.date('Y-m-d').' first day of last month';
  $dt=date_create($datestring);
  $explode =  explode('-',$dt->format('Y-m'));
  $year = $explode[0];
  $month = $explode[1];

  $received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.$month.'"
  AND lease_invoice.invoice_year = "'.$year.'" AND  lease_invoice.invoice_deleted = 0 ';
  $received_select = 'sum(invoice.invoice_amount) AS number';
  $received_table = 'invoice,lease_invoice';
  $total_last_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);

  $percentage = 0;
  $arrow = '<i class="fa fa-caret-left"></i>';
  $text = 'text-orange';
  if($total_month_invoices > $total_last_month_invoices AND $total_last_month_invoices > 0)
  {
   // this is an increase

    $percentage = (($total_month_invoices - $total_last_month_invoices)/$total_last_month_invoices) * 100;
    $arrow = '<i class="fa fa-caret-up"></i>';
    $text = 'text-green';
  }

  if($total_month_invoices < $total_last_month_invoices AND $total_month_invoices > 0 )
  {
   $percentage = (($total_last_month_invoices - $total_month_invoices)/$total_month_invoices) * 100;
   $arrow = '<i class="fa fa-caret-down"></i>';
   $text = 'text-red';
  }

  // this month
   $total_expense_this_month = $this->reports_model->get_creditor_service_amounts(NULL,date('m'),date('Y'));

  // last month

   $total_expense_last_month = $this->reports_model->get_creditor_service_amounts(NULL,$month,$year);

   $percentage2 = 0;
   $arrow2 = '<i class="fa fa-caret-left"></i>';
   $text2 = 'text-orange';
   if($total_expense_this_month > $total_expense_last_month)
   {
     // this is an increase
      $percentage2 = (($total_expense_this_month - $total_expense_last_month)/$total_expense_last_month) * 100;
      $arrow2 = '<i class="fa fa-caret-up"></i>';
      $text2 = 'text-green';
   }

   if($total_expense_this_month < $total_expense_last_month)
   {
     $percentage2 = (($total_expense_last_month - $total_expense_this_month)/$total_expense_this_month) * 100;
     $arrow2 = '<i class="fa fa-caret-down"></i>';
     $text2 = 'text-red';
   }
  // var_dump($total_last_month_invoices);die();



  $percentage3 = 0;
  $arrow3 = '<i class="fa fa-caret-left"></i>';
  $text3 = 'text-orange';
  $total_this_month_profit = $total_month_invoices - $total_expense_this_month;
  $total_last_month_profit = $total_last_month_invoices - $total_expense_last_month;

  if($total_this_month_profit > $total_last_month_profit AND $total_last_month_profit > 0)
  {
   // this is an increase
    $percentage3 = (($total_this_month_profit - $total_last_month_profit)/$total_last_month_profit) * 100;
    $arrow3 = '<i class="fa fa-caret-up"></i>';
    $text3 = 'text-green';
  }

  if($total_this_month_profit < $total_last_month_profit)
  {
   $percentage3 = (($total_last_month_profit - $total_this_month_profit)/$total_last_month_profit) * 100;

   $arrow3 = '<i class="fa fa-caret-down"></i>';
   $text3 = 'text-red';
  }

  $this_month_collection = $this->reports_model->get_amount_collected_invoice_type(NULL,date('m'),date('Y'));
  $this_month_debt = $total_month_invoices - $this_month_collection;
  $percentage4 = 0 ;
  if($total_month_invoices > 0)
  {

  $percentage4 = (($this_month_debt /$total_month_invoices) * 100);

  }

  $text4 = 'text-black';
   $color = 'bg-black';
   // $percentage4 = 40;
  if($percentage4 > 50)
  {
     $text4 = 'text-red';
     $color = 'bg-red';
  }

  if($percentage4 > 30 AND $percentage4 <= 50)
  {
     $text4 = 'text-orange';
     $color = 'bg-orange';
  }
  if($percentage4 <= 30)
  {
     $text4 = 'text-green';
     $color = 'bg-green';
  }



  ?>



  <!-- Info boxes -->
   <div class="row">
        <div class="col-md-3">

       <!-- Profile Image -->
       <div class="box box-primary">
         <div class="box-body box-profile">
           <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> -->

           <h3 class="profile-username text-center"><?php echo strtoupper($tenant_name);?></h3>

           <p class="text-muted text-center"><?php echo strtoupper($tenant_number);?></p>

           <ul class="list-group list-group-unbordered">
             <li class="list-group-item">
               <b>Allocated Properties</b> <a class="pull-right"><?php echo $total_properties?></a>
             </li>
             <li class="list-group-item">
               <b>Rental Units</b> <a class="pull-right"><?php echo $total_rental_units?></a>
             </li>
             <li class="list-group-item">
               <b>Active Leases</b> <a class="pull-right"><?php echo $occupied_rental_units?></a>
             </li>
             <li class="list-group-item">
               <b>Due Leases</b> <a class="pull-right"><?php echo $total_due_leases?></a>
             </li>
             <li class="list-group-item">
               <b>Completed Tickets</b> <a class="pull-right">0</a>
             </li>
           </ul>

           <a href="<?php echo site_url().'close-search-tenants-report'?>" class="btn btn-warning btn-block"><b>CLOSE SEACH</b></a>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->

       <!-- About Me Box -->

       <!-- /.box -->
     </div>
     <div class="col-md-9">
       <div class="col-md-12">
          <div class="col-md-3 col-xs-6">
           <!-- small box -->
           <div class="small-box bg-aqua">
             <div class="inner">
               <h3><?php echo number_format($occupied_percentage);?> %</h3>

               <p>Rental Units Occupation</p>
             </div>
             <div class="icon">
               <i class="fa fa-building"></i>
             </div>
             <a href="<?php echo site_url().'property-manager/rental-units'?>" class="small-box-footer">
               More info <i class="fa fa-arrow-circle-right"></i>
             </a>
           </div>
          </div>
           <!-- ./col -->
           <div class="col-md-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-yellow">
               <div class="inner">
                 <h3><?php echo $active_leases;?></h3>

                 <p>Active Leases</p>
               </div>
               <div class="icon">
                 <i class="ion ion-person-add"></i>
               </div>
               <a href="<?php echo site_url().'lease-manager/leases'?>" class="small-box-footer">
                 More info <i class="fa fa-arrow-circle-right"></i>
               </a>
             </div>
           </div>
           <!-- ./col -->
           <div class="col-md-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-red">
               <div class="inner">
                 <h3>0</h3>

                 <p>Complaints Feedback</p>
               </div>
               <div class="icon">
                 <i class="ion ion-pie-graph"></i>
               </div>
               <a href="#" class="small-box-footer">
                 More info <i class="fa fa-arrow-circle-right"></i>
               </a>
             </div>
           </div>
           <!-- ./col -->
           <div class="col-md-3 col-xs-6">
             <!-- small box -->
             <div class="small-box <?php echo $color;?>">
               <div class="inner">
                 <h3><?php echo number_format($percentage4)?><sup style="font-size: 20px">%</sup></h3>

                 <p>Month's Payable</p>
               </div>
               <div class="icon">
                 <i class="ion ion-stats-bars"></i>
               </div>
               <a href="<?php echo site_url().'accounts/tenants-invoices'?>" class="small-box-footer">
                 More info <i class="fa fa-arrow-circle-right"></i>
               </a>
             </div>
           </div>
       </div>
       <div class="col-md-12">
         <div class="box">
           <div class="box-header with-border">
             <h3 class="box-title">Cashflow for the past six months</h3>

             <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
               </button>
               <div class="btn-group">
                 <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                   <i class="fa fa-wrench"></i></button>
                 <ul class="dropdown-menu" role="menu">
                   <li><a href="#">Action</a></li>
                   <li><a href="#">Another action</a></li>
                   <li><a href="#">Something else here</a></li>
                   <li class="divider"></li>
                   <li><a href="#">Separated link</a></li>
                 </ul>
               </div>
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
             </div>
           </div>
           <!-- /.box-header -->
           <div class="box-body">
             <div class="row">
               <div class="col-md-12">
                 <p class="text-center">
                   <strong>Cash flow for past six months</strong>
                 </p>

                 <div class="chart">
                   <!-- Sales Chart Canvas -->
                   <canvas id="salesChart2" style="height: 180px;"></canvas>
                 </div>
                 <!-- /.chart-responsive -->
               </div>
             </div>
           </div>
           <!-- ./box-body -->
           <div class="box-footer">
             <div class="row">
               <div class="col-sm-3 col-xs-6">
                 <div class="description-block border-right">
                   <span class="description-percentage <?php echo $text;?>"><?php echo $arrow;?> <?php echo number_format($percentage,0);?>%</span>
                   <h5 class="description-header">Kes. <?php echo number_format($total_month_invoices,2)?> </h5>
                   <span class="description-text">TOTAL REVENUE</span>
                 </div>
                 <!-- /.description-block -->
               </div>
               <!-- /.col -->
               <div class="col-sm-3 col-xs-6">
                 <div class="description-block border-right">
                     <span class="description-percentage <?php echo $text2;?>"><?php echo $arrow2;?> <?php echo number_format($percentage2,0);?>%</span>
                   <h5 class="description-header">Kes. <?php echo number_format($total_expense_this_month,2)?></h5>
                   <span class="description-text">TOTAL EXPENSE</span>
                 </div>
                 <!-- /.description-block -->
               </div>
               <!-- /.col -->
               <div class="col-sm-3 col-xs-6">
                 <div class="description-block border-right">
                   <span class="description-percentage <?php echo $text3;?>"><?php echo $arrow3;?> <?php echo number_format($percentage3,0);?>%</span>
                   <h5 class="description-header">Kes. <?php echo number_format($total_this_month_profit,2)?></h5>
                   <span class="description-text">TOTAL PROFIT</span>
                 </div>
                 <!-- /.description-block -->
               </div>
               <!-- /.col -->
               <div class="col-sm-3 col-xs-6">
                 <div class="description-block">
                   <span class="description-percentage <?php echo $text4;?>"><i class="fa fa-caret-down"></i> <?php echo number_format($percentage4,0)?>%</span>
                   <h5 class="description-header">Kes. <?php echo number_format($this_month_debt,2)?></h5>
                   <span class="description-text">GOAL COMPLETIONS</span>
                 </div>
                 <!-- /.description-block -->
               </div>
             </div>
             <!-- /.row -->
           </div>
           <!-- /.box-footer -->
         </div>
       </div>
     </div>
  </div>
   <!-- /.row -->

   <div class="row">
     <div class="col-md-3">
    </div>
     <div class="col-md-9">
       <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Line Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
       <!-- /.box -->
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->

   <!-- Main row -->
   <div class="row">
     <!-- Left col -->
     <div class="col-md-12">
       <!-- MAP & BOX PANE -->

       <!-- /.box -->
       <div class="row">


         <div class="col-md-12">
           <!-- USERS LIST -->
           <div class="box box-danger">
             <div class="box-header with-border">
               <h3 class="box-title">Assigned Properties</h3>

               <div class="box-tools pull-right">
                 <span class="label label-danger">8 New Members</span>
                 <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                 </button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                 </button>
               </div>
             </div>
             <!-- /.box-header -->
             <div class="box-body no-padding">
               <ul class="users-list clearfix">
                    <?php echo $assigned_properties;?>
                 <!-- <li>
                   <img src="dist/img/user3-128x128.jpg" alt="User Image">
                   <a class="users-list-name" href="#">Nadia</a>
                   <span class="users-list-date">15 Jan</span>
                 </li> -->
               </ul>
               <!-- /.users-list -->
             </div>
             <!-- /.box-body -->
             <div class="box-footer text-center">
               <a href="javascript:void(0)" class="uppercase">View All Users</a>
             </div>
             <!-- /.box-footer -->
           </div>
           <!--/.box -->
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->


     </div>
     <!-- /.col -->

     <!-- /.col -->
  </div>
   <!-- /.row -->
  <?php
  }
  ?>

  <script type="text/javascript">
   $(function () {

   'use strict';

   /* ChartJS
    * -------
    * Here we will create a few charts using ChartJS
    */

   // -----------------------
   // - MONTHLY SALES CHART -
   // -----------------------

   // Get context with jQuery - using jQuery's .get() method.
   var salesChartCanvas = $('#salesChart2').get(0).getContext('2d');
   // This will get the first returned node in the jQuery collection.
   var salesChart       = new Chart(salesChartCanvas);

   var salesChartData = {
     labels  : [<?php echo $total_labels;?>],
     datasets: [
       {
         label               : 'Expense',
         fillColor           : 'rgb(210, 214, 222)',
         strokeColor         : 'rgb(210, 214, 222)',
         pointColor          : 'rgb(210, 214, 222)',
         pointStrokeColor    : '#c1c7d1',
         pointHighlightFill  : '#fff',
         pointHighlightStroke: 'rgb(220,220,220)',
         data                : [<?php echo $chartExpenses?>]
       },
       {
         label               : 'Income / Cash Flow',
         fillColor           : 'rgba(60,141,188,0.9)',
         strokeColor         : 'rgba(60,141,188,0.8)',
         pointColor          : '#3b8bba',
         pointStrokeColor    : 'rgba(60,141,188,1)',
         pointHighlightFill  : '#fff',
         pointHighlightStroke: 'rgba(60,141,188,1)',
         data                : [<?php echo $chartIncomes?>]
       }
     ]
   };

   var salesChartOptions = {
     // Boolean - If we should show the scale at all
     showScale               : true,
     // Boolean - Whether grid lines are shown across the chart
     scaleShowGridLines      : false,
     // String - Colour of the grid lines
     scaleGridLineColor      : 'rgba(0,0,0,.05)',
     // Number - Width of the grid lines
     scaleGridLineWidth      : 1,
     // Boolean - Whether to show horizontal lines (except X axis)
     scaleShowHorizontalLines: true,
     // Boolean - Whether to show vertical lines (except Y axis)
     scaleShowVerticalLines  : true,
     // Boolean - Whether the line is curved between points
     bezierCurve             : true,
     // Number - Tension of the bezier curve between points
     bezierCurveTension      : 0.3,
     // Boolean - Whether to show a dot for each point
     pointDot                : false,
     // Number - Radius of each point dot in pixels
     pointDotRadius          : 4,
     // Number - Pixel width of point dot stroke
     pointDotStrokeWidth     : 1,
     // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
     pointHitDetectionRadius : 20,
     // Boolean - Whether to show a stroke for datasets
     datasetStroke           : true,
     // Number - Pixel width of dataset stroke
     datasetStrokeWidth      : 2,
     // Boolean - Whether to fill the dataset with a color
     datasetFill             : true,
     // String - A legend template
     legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
     // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
     maintainAspectRatio     : true,
     // Boolean - whether to make the chart responsive to window resizing
     responsive              : true
   };

   // Create the line chart
   salesChart.Line(salesChartData, salesChartOptions);






   /* ChartJS
      * -------
      * Here we will create a few charts using ChartJS
      */

     //--------------
     //- AREA CHART -
     //--------------

     // Get context with jQuery - using jQuery's .get() method.
     // var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
     // This will get the first returned node in the jQuery collection.
     // var areaChart       = new Chart(areaChartCanvas)

     var areaChartData = {
       labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
       datasets: [
         {
           label               : 'Electronics',
           fillColor           : 'rgba(210, 214, 222, 1)',
           strokeColor         : 'rgba(210, 214, 222, 1)',
           pointColor          : 'rgba(210, 214, 222, 1)',
           pointStrokeColor    : '#c1c7d1',
           pointHighlightFill  : '#fff',
           pointHighlightStroke: 'rgba(220,220,220,1)',
           data                : [65, 59, 80, 81, 56, 55, 40]
         },
         {
           label               : 'Digital Goods',
           fillColor           : 'rgba(60,141,188,0.9)',
           strokeColor         : 'rgba(60,141,188,0.8)',
           pointColor          : '#3b8bba',
           pointStrokeColor    : 'rgba(60,141,188,1)',
           pointHighlightFill  : '#fff',
           pointHighlightStroke: 'rgba(60,141,188,1)',
           data                : [28, 48, 40, 19, 86, 27, 90]
         }
       ]
     }

     var areaChartOptions = {
       //Boolean - If we should show the scale at all
       showScale               : true,
       //Boolean - Whether grid lines are shown across the chart
       scaleShowGridLines      : false,
       //String - Colour of the grid lines
       scaleGridLineColor      : 'rgba(0,0,0,.05)',
       //Number - Width of the grid lines
       scaleGridLineWidth      : 1,
       //Boolean - Whether to show horizontal lines (except X axis)
       scaleShowHorizontalLines: true,
       //Boolean - Whether to show vertical lines (except Y axis)
       scaleShowVerticalLines  : true,
       //Boolean - Whether the line is curved between points
       bezierCurve             : true,
       //Number - Tension of the bezier curve between points
       bezierCurveTension      : 0.3,
       //Boolean - Whether to show a dot for each point
       pointDot                : false,
       //Number - Radius of each point dot in pixels
       pointDotRadius          : 4,
       //Number - Pixel width of point dot stroke
       pointDotStrokeWidth     : 1,
       //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
       pointHitDetectionRadius : 20,
       //Boolean - Whether to show a stroke for datasets
       datasetStroke           : true,
       //Number - Pixel width of dataset stroke
       datasetStrokeWidth      : 2,
       //Boolean - Whether to fill the dataset with a color
       datasetFill             : true,
       //String - A legend template
       legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
       //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
       maintainAspectRatio     : true,
       //Boolean - whether to make the chart responsive to window resizing
       responsive              : true
     }

     //Create the line chart
     // areaChart.Line(areaChartData, areaChartOptions);
     // alert("dsdasda");
     //-------------
     //- LINE CHART -
     //--------------
     var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
     var lineChart                = new Chart(lineChartCanvas)
     var lineChartOptions         = areaChartOptions
     lineChartOptions.datasetFill = false
     lineChart.Line(areaChartData, lineChartOptions)


   });

  </script>
