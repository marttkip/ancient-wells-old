<?php

class Creditors_model extends CI_Model 
{	
	
	/*
	*	Add a new creditor
	*
	*/
	public function add_creditor()
	{
		$creditor_type_id = $this->input->post('creditor_type_id');

		if(isset($creditor_type_id))
		{
			$creditor_type_id = 1;
		}
		else
		{
			$creditor_type_id = 0;
		}
		$data = array(
			'creditor_name'=>$this->input->post('creditor_name'),
			'creditor_email'=>$this->input->post('creditor_email'),
			'creditor_phone'=>$this->input->post('creditor_phone'),
			'creditor_location'=>$this->input->post('creditor_location'),
			'creditor_building'=>$this->input->post('creditor_building'),
			'creditor_floor'=>$this->input->post('creditor_floor'),
			'creditor_address'=>$this->input->post('creditor_address'),
			'creditor_post_code'=>$this->input->post('creditor_post_code'),
			'creditor_city'=>$this->input->post('creditor_city'),
			'opening_balance'=>$this->input->post('opening_balance'),
			'creditor_contact_person_name'=>$this->input->post('creditor_contact_person_name'),
			'creditor_contact_person_onames'=>$this->input->post('creditor_contact_person_onames'),
			'creditor_contact_person_phone1'=>$this->input->post('creditor_contact_person_phone1'),
			'creditor_contact_person_phone2'=>$this->input->post('creditor_contact_person_phone2'),
			'creditor_contact_person_email'=>$this->input->post('creditor_contact_person_email'),
			'creditor_description'=>$this->input->post('creditor_description'),
			'branch_code'=>$this->session->userdata('branch_code'),
			'created_by'=>$this->session->userdata('creditor_id'),
			'debit_id'=>$this->input->post('debit_id'),
			'modified_by'=>$this->session->userdata('creditor_id'),
			'creditor_type_id'=>$creditor_type_id,
			'created'=>date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('creditor', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing creditor
	*	@param string $image_name
	*	@param int $creditor_id
	*
	*/
	public function edit_creditor($creditor_id)
	{
		$data = array(
			'creditor_name'=>$this->input->post('creditor_name'),
			'creditor_email'=>$this->input->post('creditor_email'),
			'creditor_phone'=>$this->input->post('creditor_phone'),
			'creditor_location'=>$this->input->post('creditor_location'),
			'creditor_building'=>$this->input->post('creditor_building'),
			'creditor_floor'=>$this->input->post('creditor_floor'),
			'creditor_address'=>$this->input->post('creditor_address'),
			'creditor_post_code'=>$this->input->post('creditor_post_code'),
			'creditor_city'=>$this->input->post('creditor_city'),
			'opening_balance'=>$this->input->post('opening_balance'),
			'creditor_contact_person_name'=>$this->input->post('creditor_contact_person_name'),
			'creditor_contact_person_onames'=>$this->input->post('creditor_contact_person_onames'),
			'creditor_contact_person_phone1'=>$this->input->post('creditor_contact_person_phone1'),
			'creditor_contact_person_phone2'=>$this->input->post('creditor_contact_person_phone2'),
			'creditor_contact_person_email'=>$this->input->post('creditor_contact_person_email'),
			'creditor_description'=>$this->input->post('creditor_description'),
			'debit_id'=>$this->input->post('debit_id'),
			'modified_by'=>$this->session->userdata('creditor_id'),
		);
		
		$this->db->where('creditor_id', $creditor_id);
		if($this->db->update('creditor', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();
		
		return $query;
	}

	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_personnel_names($personnel_id)
	{
		//retrieve all users
		$this->db->from('personnel');
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_creditors($table, $where, $per_page, $page, $order = 'creditor_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	/*
	*	Retrieve all creditor
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_creditors_account($table, $where, $per_page, $page, $order = 'creditor_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_creditors_detail_summary($where, $table)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('creditor_name', 'ASC');
		$query = $this->db->get('');
		
		return $query;
	}

	public function calculate_balance_brought_forward($date_from,$creditor_id)
	{
		$this->db->select('(
(SELECT SUM(creditor_account_amount) FROM creditor_account WHERE creditor_account_status = 1 AND transaction_type_id = 1 AND creditor_account_date < \''.$date_from.'\' AND creditor_id= '.$creditor_id.')
-
(SELECT SUM(creditor_account_amount) FROM creditor_account WHERE creditor_account_status = 1 AND transaction_type_id = 2 AND creditor_account_date < \''.$date_from.'\' AND creditor_id = '.$creditor_id.')
) AS balance_brought_forward', FALSE); 
		$this->db->where('creditor_account_date < \''.$date_from.'\' AND creditor_id = '.$creditor_id.'' );
		$this->db->group_by('balance_brought_forward');
		$query = $this->db->get('creditor_account');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->balance_brought_forward;
		}
		
		else
		{
			return 0;
		}
	}
	
	public function get_creditor_account($where, $table)
	{
		$this->db->select('*');
		//$this->db->join('account', 'creditor_account.account_id = account.account_id', 'left');
		$this->db->where($where);
		$this->db->order_by('creditor_account_date', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}

	public function get_creditor_transactions($where, $table)
	{
		$this->db->select('*');
		$this->db->where($where);
		$this->db->group_by('transaction_code', 'ASC');
		$query = $this->db->get($table);
		
		return $query;
	}
	
	public function record_creditor_account($creditor_id)
	{
		$transaction_type = $this->input->post('transaction_type_id');
		$account = array(
			'account_to_id'=>12,//$this->input->post('account_to_id'),
			'account_from_id'=>$this->input->post('account_from_id'),
			'invoice_amount'=>$this->input->post('creditor_account_amount'),
			'account_invoice_description'=>$this->input->post('creditor_account_description'),
            'account_to_type'=>2,//$this->input->post('transaction_type_id'),
            'invoice_date'=>$this->input->post('creditor_account_date'),
            'created_by'=>$this->session->userdata('personnel_id'),
            'invoice_number'=>$this->input->post('transaction_code'),
            'created'=>date('Y-m-d')
			);
		// var_dump($account); die();
		if($this->db->insert('account_invoices',$account))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}

	public function record_provider_account($provider_id)
	{
		$transaction_type = $this->input->post('transaction_type_id');
		$account = array(
			'account_to_id'=>12,//$this->input->post('account_to_id'),
			'account_from_id'=>$this->input->post('account_from_id'),
			'invoice_amount'=>$this->input->post('creditor_account_amount'),
			'account_invoice_description'=>$this->input->post('creditor_account_description'),
            'account_to_type'=>3,//$this->input->post('transaction_type_id'),
            'invoice_date'=>$this->input->post('creditor_account_date'),
            'created_by'=>$this->session->userdata('personnel_id'),
            'invoice_number'=>$this->input->post('transaction_code'),
            'created'=>date('Y-m-d')
			);
		// var_dump($account); die();
		if($this->db->insert('account_invoices',$account))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}


	public function update_provider_account($provider_id)
	{
		$account = array(
			'provider_id'=>$provider_id,//$this->input->post('account_to_id'),
			'opening_balance'=>$this->input->post('opening_balance'),
			'debit_id'=>$this->input->post('debit_id'),
			'created'=>$this->input->post('start_date')
			);

		// check if it exists

		$this->db->where('provider_id',$provider_id);
		$query = $this->db->get('provider_account');

		if($query->num_rows() > 0)
		{
			// update
			$this->db->where('provider_id',$provider_id);
			if($this->db->update('provider_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			// insert
			if($this->db->insert('provider_account',$account))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		// var_dump($account); die();
		
		
	}

	public function get_invoice_total($creditor_id)
	{
		$invoice_total = 0;

		$this->db->select(' SUM(creditor_account_amount) AS total_invoice');
		$this->db->where('creditor_account_status = 1 AND transaction_type_id = 2 AND creditor_account_delete = 0 AND creditor_id = '.$creditor_id);
		$query = $this->db->get ('creditor_account'); 
		
		$invoice_total_row = $query->row();
		$invoice_total = $invoice_total_row->total_invoice;

		return $invoice_total;

	}
	public function get_payments_total($creditor_id)
	{
		$payment_total = 0;

		$this->db->select(' SUM(creditor_account_amount) AS total_payment');
		$this->db->where('creditor_account_status = 1 AND transaction_type_id = 1 AND creditor_account_delete = 0 AND creditor_id = '.$creditor_id);
		$query = $this->db->get ('creditor_account'); 
		
		$payment_total_row = $query->row();
		$payment_total = $payment_total_row->total_payment;

		return $payment_total;

	}
	public function get_statement_value($creditor_id,$date,$value)
	{
		// invoices
		$invoice = '';
		$first_date = date('Y-m').'-01';
		if($value == 1)
		{
			$invoice = ' AND invoice_date >= "'.$first_date.'" AND invoice_date <= "'.$date.'" ';

			$balance = ' AND payment_date >= "'.$first_date.'" AND payment_date <= "'.$date.'" ';
		}
		else if($value == 2)
		{

			$three_months = date('Y-m-d', strtotime('-2 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date < "'.$first_date.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$first_date.'" ';
		}
		else if($value == 3)
		{

			$three_months = date('Y-m-d', strtotime('-3 months'));
			$send_first = date('Y-m-01', strtotime('-2 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_first.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_first.'" ';
		}

		else if($value == 4)
		{

			$three_months = date('Y-m-d', strtotime('-4 months'));
			$send_second = date('Y-m-01', strtotime('-3 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_second.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_second.'" ';
		}
		else if($value == 5)
		{

			$three_months = date('Y-m-d', strtotime('-5 months'));
			$send_fourth = date('Y-m-01', strtotime('-4 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_fourth.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_fourth.'" ';
		}
		else if($value == 6)
		{
			$three_months = date('Y-m-d', strtotime('-6 months'));
		    $send_third = date('Y-m-01', strtotime('-5 months'));
			$invoice = ' AND invoice_date <= "'.$send_third.'" ';
			$balance = ' AND payment_date <= "'.$send_third.'" ';
		}


		
		

		$this->db->select(' SUM(invoice_amount) AS total_invoice');
		$this->db->where('account_to_type = 2 AND account_invoice_deleted = 0  AND account_from_id = '.$creditor_id.' '.$invoice);
		$query = $this->db->get ('account_invoices');

		$invoice_total = 0; 		
		if($query->num_rows() > 0)
		{
			$invoice_total_row = $query->row();
			$invoice_total = $invoice_total_row->total_invoice;
		}

		// payments
		$payment_total = 0;
		$this->db->select(' SUM(amount_paid) AS total_payment');
		$this->db->where('account_to_type = 2 AND account_payment_deleted = 0  AND account_to_id = '.$creditor_id.''.$payment_search.' '.$balance);
		$query_payments = $this->db->get ('account_payments'); 
		
		if($query_payments->num_rows() > 0)
		{
			$payment_total_row = $query_payments->row();
			$payment_total = $payment_total_row->total_payment;
		}

		$this->db->where('creditor_id = '.$creditor_id);
		$creditor = $this->db->get('creditor');
		$balance_amount = 0;
		if($creditor->num_rows() > 0)
		{
			$row = $creditor->row();
			$creditor_name = $row->creditor_name;
			$opening_balance = $row->opening_balance;
			$created = $row->created;
			$debit_id = $row->debit_id;

			if($debit_id == 1)
			{
				$invoice_total = $invoice_total + $opening_balance;
			}
			else
			{
				$payment_total = $payment_total + $opening_balance;
			}


		}

		$amount = $invoice_total - $payment_total;

		if($amount < 0)
		{
			$amount = -$amount;
		}

		return $amount;

	}


	public function get_provider_statement_value($provider_id,$date,$value)
	{
		// invoices
		$invoice = '';
		$first_date = date('Y-m').'-01';
		if($value == 1)
		{
			$invoice = ' AND invoice_date >= "'.$first_date.'" AND invoice_date <= "'.$date.'" ';

			$balance = ' AND payment_date >= "'.$first_date.'" AND payment_date <= "'.$date.'" ';
		}
		else if($value == 2)
		{

			$three_months = date('Y-m-d', strtotime('-2 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date < "'.$first_date.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$first_date.'" ';
		}
		else if($value == 3)
		{

			$three_months = date('Y-m-d', strtotime('-3 months'));
			$send_first = date('Y-m-01', strtotime('-2 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_first.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_first.'" ';
		}

		else if($value == 4)
		{

			$three_months = date('Y-m-d', strtotime('-4 months'));
			$send_second = date('Y-m-01', strtotime('-3 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_second.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_second.'" ';
		}
		else if($value == 5)
		{

			$three_months = date('Y-m-d', strtotime('-5 months'));
			$send_fourth = date('Y-m-01', strtotime('-4 months'));
			$invoice = ' AND invoice_date >= "'.$three_months.'" AND invoice_date <= "'.$send_fourth.'" ';
			$balance = ' AND payment_date >= "'.$three_months.'" AND payment_date <= "'.$send_fourth.'" ';
		}
		else if($value == 6)
		{
			$three_months = date('Y-m-d', strtotime('-6 months'));
		    $send_third = date('Y-m-01', strtotime('-5 months'));
			$invoice = ' AND invoice_date <= "'.$send_third.'" ';
			$balance = ' AND payment_date <= "'.$send_third.'" ';
		}


		
		

		$this->db->select(' SUM(invoice_amount) AS total_invoice');
		$this->db->where('account_to_type = 3 AND account_invoice_deleted = 0  AND account_from_id = '.$provider_id.' '.$invoice);
		$query = $this->db->get ('account_invoices');

		$invoice_total = 0; 		
		if($query->num_rows() > 0)
		{
			$invoice_total_row = $query->row();
			$invoice_total = $invoice_total_row->total_invoice;
		}

		// payments
		$payment_total = 0;
		$this->db->select(' SUM(amount_paid) AS total_payment');
		$this->db->where('account_to_type = 3 AND account_payment_deleted = 0  AND account_to_id = '.$provider_id.''.$payment_search.' '.$balance);
		$query_payments = $this->db->get ('account_payments'); 
		
		if($query_payments->num_rows() > 0)
		{
			$payment_total_row = $query_payments->row();
			$payment_total = $payment_total_row->total_payment;
		}

		$this->db->where('provider_id = '.$provider_id);
		$creditor = $this->db->get('provider_account');
		$balance_amount = 0;
		if($creditor->num_rows() > 0)
		{
			$row = $creditor->row();
			$opening_balance = $row->opening_balance;
			$created = $row->created;
			$debit_id = $row->debit_id;

			if($debit_id == 1)
			{
				$invoice_total = $invoice_total + $opening_balance;
			}
			else
			{
				$payment_total = $payment_total + $opening_balance;
			}


		}

		$amount = $invoice_total - $payment_total;

		if($amount < 0)
		{
			$amount = -$amount;
		}

		return $amount;

	}


	public function get_creditor3($creditor_account_id)
	
	  {
		//retrieve all users
		$this->db->from('creditor_account');
		$this->db->select('*');
		$this->db->where('creditor_account_id = 1'.$creditor_account_id);
		$query = $this->db->get();
		
		return $query;    	
 
     }	
		public function delete_creditor($creditor_account_id)
		{
			$array = array(
				'creditor_account_delete'=>1
			);
			$this->db->where('creditor_account_id', $creditor_account_id);
			if($this->db->update('creditor_account', $array))
			{
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		public function get_all_creditor_invoices($creditor_id)
		{
			$search = $this->session->userdata('creditor_invoice_search');

			if(!empty($search))
			{
				$invoice_search = $search;
			}
			else
			{
				$invoice_search = '';
			}
			
			$this->db->from('account_invoices');
			$this->db->select('*');
			$this->db->where('account_to_type = 2 AND account_invoice_deleted = 0  AND account_from_id = '.$creditor_id.''.$invoice_search);
			$this->db->order_by('invoice_date','ASC');
			$this->db->group_by('invoice_number');
			$query = $this->db->get();
			return $query;
		}

		

		public function get_all_provider_invoices($provider_id)
		{
			$search = $this->session->userdata('provider_invoice_search');

			if(!empty($search))
			{
				$invoice_search = $search;
			}
			else
			{
				$invoice_search = '';
			}
			
			$this->db->from('account_invoices');
			$this->db->select('*');
			$this->db->where('account_to_type = 3 AND account_invoice_deleted = 0  AND account_from_id = '.$provider_id.''.$invoice_search);
			$this->db->order_by('invoice_date','ASC');
			$this->db->group_by('invoice_number');
			$query = $this->db->get();
			return $query;
		}

		public function get_all_lab_works($month,$year,$provider_id)
		{
			$this->db->from('visit,visit_lab_work');
			$this->db->select('SUM(visit_lab_work.amount_to_charge) AS total_charged_amount');
			$this->db->where('visit.visit_id = visit_lab_work.visit_id AND visit.visit_delete = 0 AND visit.personnel_id = '.$provider_id.' AND MONTH(visit.visit_date) = "'.$month.'" AND YEAR(visit_date) = "'.$year.'" ');
			$query = $this->db->get();

			$result = $query->row();

			return $result->total_charged_amount;
		}

		public function get_all_provider_work_done($provider_id)
		{
			$search = $this->session->userdata('provider_invoice_search');

			if(!empty($search))
			{
				$invoice_search = $search;
			}
			else
			{
				$invoice_search = '';
			}
			
			$this->db->from('visit,visit_charge');
			$this->db->select('visit_date,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_charged_amount');
			$this->db->where('visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit.personnel_id = '.$provider_id.'');
			$this->db->order_by('YEAR(visit.visit_date),MONTH(visit.visit_date)','ASC');
			$this->db->group_by('YEAR(visit.visit_date),MONTH(visit.visit_date)');
			$query = $this->db->get();
			return $query;
		}



		public function get_all_payments_creditor($creditor_id)
		{
			$search = $this->session->userdata('creditor_payment_search');

			if(!empty($search))
			{
				$payment_search = $search;
			}
			else
			{
				$payment_search = '';
			}
			$this->db->from('account_payments');
			$this->db->select('*');
			$this->db->where('account_to_type = 2 AND account_payment_deleted = 0  AND account_to_id = '.$creditor_id.''.$payment_search);
			$this->db->order_by('payment_date','ASC');
			$query = $this->db->get();
			return $query;
		}


		public function get_all_payments_provider($provider_id)
		{
			$search = $this->session->userdata('provider_payment_search');

			if(!empty($search))
			{
				$payment_search = $search;
			}
			else
			{
				$payment_search = '';
			}
			$this->db->from('account_payments');
			$this->db->select('*');
			$this->db->where('account_to_type = 3 AND account_payment_deleted = 0  AND account_to_id = '.$provider_id.''.$payment_search);
			$this->db->order_by('payment_date','ASC');
			$query = $this->db->get();
			return $query;
		}

		public function get_invoice_brought_forward($creditor_id,$invoice_search)
		{
			
			$this->db->from('account_invoices');
			$this->db->select('SUM(invoice_amount) AS amount');
			$this->db->where('account_to_type = 2 AND account_invoice_deleted = 0  AND account_from_id = '.$creditor_id.''.$invoice_search);
			$this->db->order_by('invoice_date','ASC');
			$this->db->group_by('invoice_number');
			$query = $this->db->get();
			$amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$amount = $value->amount;
				}
			}
			return $amount;
			
		}

		public function get_provider_invoice_brought_forward($provider_id,$invoice_search)
		{
			
			$this->db->from('account_invoices');
			$this->db->select('SUM(invoice_amount) AS amount');
			$this->db->where('account_to_type = 3 AND account_invoice_deleted = 0  AND account_from_id = '.$provider_id.''.$invoice_search);
			$this->db->order_by('invoice_date','ASC');
			$this->db->group_by('invoice_number');
			$query = $this->db->get();
			$amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$amount = $value->amount;
				}
			}
			return $amount;
			
		}

		public function get_payment_brought_forward($creditor_id,$payment_search)
		{
			
			$this->db->from('account_payments');
			$this->db->select('SUM(amount_paid) AS amount');
			$this->db->where('account_to_type = 2 AND account_payment_deleted = 0  AND account_to_id = '.$creditor_id.''.$payment_search);
			$this->db->order_by('payment_date','ASC');
			$query = $this->db->get();
			$amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$amount = $value->amount;
				}
			}
			return $amount;
			
		}

		public function get_provider_payment_brought_forward($provider_id,$payment_search)
		{
			
			$this->db->from('account_payments');
			$this->db->select('SUM(amount_paid) AS amount');
			$this->db->where('account_to_type = 3 AND account_payment_deleted = 0  AND account_to_id = '.$provider_id.''.$payment_search);
			$this->db->order_by('payment_date','ASC');
			$query = $this->db->get();
			$amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$amount = $value->amount;
				}
			}
			return $amount;
			
		}


		// public function get_provider_payment_brought_forward($provider_id,$payment_search)
		// {
			
		// 	$this->db->from('account_payments');
		// 	$this->db->select('SUM(amount_paid) AS amount');
		// 	$this->db->where('account_to_type = 3 AND account_payment_deleted = 0  AND account_to_id = '.$provider_id.''.$payment_search);
		// 	$this->db->order_by('payment_date','ASC');
		// 	$query = $this->db->get();
		// 	$amount = 0;
		// 	if($query->num_rows() > 0)
		// 	{
		// 		foreach ($query->result() as $key => $value) {
		// 			# code...
		// 			$amount = $value->amount;
		// 		}
		// 	}
		// 	return $amount;
			
		// }

		public function get_balance_brought_forward($creditor_id)
		{
			$invoice_search = $this->session->userdata('balance_invoice_search');
			$payment_search = $this->session->userdata('balance_payment_search');

			if(!empty($invoice_search))
			{
				$invoice_total = $this->get_invoice_brought_forward($creditor_id,$invoice_search);
				$payment_total = $this->get_payment_brought_forward($creditor_id,$payment_search);

				$balance = $payment_total - $invoice_total;

				return $balance;
			}
			else
			{
				return FALSE;
			}
		}


		public function get_provider_balance_brought_forward($provider_id)
		{
			$invoice_search = $this->session->userdata('balance_invoice_search');
			$payment_search = $this->session->userdata('balance_payment_search');

			if(!empty($invoice_search))
			{
				$invoice_total = $this->get_provider_invoice_brought_forward($provider_id,$invoice_search);
				$payment_total = $this->get_provider_payment_brought_forward($provider_id,$payment_search);

				$balance = $payment_total - $invoice_total;

				return $balance;
			}
			else
			{
				return FALSE;
			}
		}

		public function get_all_suppplier_invoices($creditor_id)
		{
			$this->db->where('orders.is_store = 0 AND orders.order_approval_status = 7 AND orders.supplier_invoice_number IS NOT NULL AND orders.supplier_id ='.$creditor_id);
			$this->db->select('orders.supplier_invoice_number AS invoice_number,orders.supplier_invoice_date AS invoice_date,orders.order_id');
			$query = $this->db->get('orders');

			return $query;
		}

		public function get_all_suppplier_credit_note($creditor_id)
		{
			$this->db->where('orders.is_store = 3 AND orders.order_approval_status = 7 AND orders.supplier_invoice_number IS NOT NULL AND orders.supplier_id ='.$creditor_id);
			$this->db->select('orders.reference_number AS invoice_number,orders.supplier_invoice_date AS invoice_date,orders.order_id');
			$query = $this->db->get('orders');

			return $query;
		}
		public function get_total_supplied_invoice($order_id)
		{
			$this->db->where('order_supplier.order_id ='.$order_id);
			$this->db->select('SUM(order_supplier.total_amount) AS invoice_amount');
			$query = $this->db->get('order_supplier');
			$invoice_amount = 0;
			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$invoice_amount = $value->invoice_amount;
				}
			}
			return $invoice_amount;
		}
		public function get_creditor_statement($creditor_id)
		{

			$creditor_query = $this->creditors_model->get_opening_creditor_balance($creditor_id);
			$bills = $this->get_all_creditor_invoices($creditor_id);

			$bills_query = $this->get_all_suppplier_invoices($creditor_id);
			$credit_note_query = $this->get_all_suppplier_credit_note($creditor_id);
			// var_dump($bills_query); 
			$payments = $this->get_all_payments_creditor($creditor_id);

			$brought_forward_balance = $this->get_balance_brought_forward($creditor_id);

			


			$x=0;

			$bills_result = '';
			$last_date = '';
			$current_year = date('Y');
			$total_invoices = $bills->num_rows();
			$invoices_count = 0;
			$total_invoice_balance = 0;
			$total_arrears = 0;
			$total_payment_amount = 0;
			$result = '';
			$total_pardon_amount = 0;



			if($creditor_query->num_rows() > 0)
			{
				$row = $creditor_query->row();
				$opening_balance = $row->opening_balance;
				$created = $row->created;
				$debit_id = $row->debit_id;
				// var_dump($debit_id); die();
				if($debit_id == 2)
				{
					// this is deni
					$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($created)).' </td>
										<td>OPENING BALANCE</td>
										<td></td>
										<td>'.number_format($opening_balance, 2).'</td>
										<td></td>
										<td></td>
									</tr> 
								';
					$total_payment_amount = $opening_balance;

				}
				else
				{
					// this is a prepayment
					$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($created)).' </td>
										<td>OPENING BALANCE</td>
										<td></td>
										<td></td>
										<td>'.number_format($opening_balance, 2).'</td>
										<td></td>
									</tr> 
								';
					$total_invoice_balance = $opening_balance;
				}
			}
			

			if($brought_forward_balance == FALSE)
			{
				$result .='';
			}

			else
			{
				$search_title = $this->session->userdata('creditor_search_title');
				if($brought_forward_balance < 0)
				{
					$positive = -$brought_forward_balance;
					$result .= 
								'
									<tr>
										<td colspan=3> B/F</td>
										<td>'.number_format($positive, 2).'</td>
										<td></td>
									</tr> 
								';
					$total_invoice_balance += $positive;

				}
				else
				{
					$result .= 
								'
									<tr>
										<td > B/F</td>
										<td></td>
										<td>'.number_format($brought_forward_balance, 2).'</td>
									</tr> 
								';


					$total_invoice_balance += $brought_forward_balance;
				}
			}


			if($bills->num_rows() > 0)
			{
				foreach ($bills->result() as $key_bills) {
					# code...
					$invoice_date = $key_bills->invoice_date;
					$invoice_number = $key_bills->invoice_number;
					$invoice_amount = $key_bills->invoice_amount;
					$invoice_explode = explode('-', $invoice_date);
					$invoice_year = $invoice_explode[0];
					$invoice_month = $invoice_explode[1];
					$account_invoice_description = $key_bills->account_invoice_description;
					$account_to_id = $key_bills->account_to_id;
					$account_from_id = $key_bills->account_from_id;
					$account_invoice_id = $key_bills->account_invoice_id;
					// var_dump($bills->result()); die();
					$invoices_count++;
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_explode = explode('-', $payment_date);
							$payment_year = $payment_explode[0];
							$payment_month = $payment_explode[1];
							$payment_amount = $payments_key->amount_paid;
							$account_payment_id = $payments_key->account_payment_id;


							if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// var_dump($payment_year); die();
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>PAYMENT</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
												<td></td>
											<td><a href="'.site_url().'delete-creditor-payment-entry/'.$account_payment_id.'/'.$creditor_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}


					
					//display disbursment if cheque amount > 0
					if($invoice_amount != 0)
					{
						$total_arrears += $invoice_amount;
						$total_invoice_balance += $invoice_amount;
						$account_name = $this->get_account_name($account_to_id);
						// if($invoice_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($invoice_date)).' </td>
									<td>'.strtoupper($invoice_number).'</td>
									<td>'.$account_invoice_description.'</td>
									<td></td>
									<td>'.number_format($invoice_amount, 2).'</td>
									<td><a href="'.site_url().'delete-creditor-invoice-entry/'.$account_invoice_id.'/'.$creditor_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
								</tr> 
							';
						// }
					}

					if($bills_query->num_rows() > 0)
					{
						foreach ($bills_query->result() as $supplier) {
							# code...
							$invoice_date_bill = $supplier->invoice_date;
							$invoice_number = $supplier->invoice_number;
							$order_id = $supplier->order_id;
							$invoice_amount = $this->get_total_supplied_invoice($order_id);
							$invoice_explode = explode('-', $invoice_date_bill);

							if(($invoice_date_bill <= $invoice_date) && ($invoice_date_bill > $last_date) && ($invoice_amount > 0))
							{
								$total_arrears += $invoice_amount;
								$total_invoice_balance += $invoice_amount;
							
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
											<td>'.strtoupper($invoice_number).'</td>
											<td>Drug Purchases</td>
											<td></td>
											<td>'.number_format($invoice_amount, 2).'</td>
											<td></td>
										</tr> 
									';
								
							}
						}

					}

					if($credit_note_query->num_rows() > 0)
					{
						foreach ($credit_note_query->result() as $credit_note) {
							# code...
							$invoice_date_bill = $credit_note->invoice_date;
							$invoice_number = $credit_note->invoice_number;
							$order_id = $credit_note->order_id;
							$credit_note_amount = $this->get_total_supplied_invoice($order_id);
							$invoice_explode = explode('-', $invoice_date_bill);

							if(($invoice_date_bill <= $invoice_date) && ($invoice_date_bill > $last_date) && ($credit_note_amount > 0))
							{
								$total_arrears -= $credit_note_amount;
								$total_payment_amount += $credit_note_amount;
							
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
											<td>'.strtoupper($invoice_number).'</td>
											<td>Credit Note</td>
											<td>'.number_format($credit_note_amount, 2).'</td>
											<td></td>
											<td></td>
										</tr> 
									';
								
							}
						}

					}

							
					//check if there are any more payments
					if($total_invoices == $invoices_count)
					{
						//get all loan deductions before date
						if($payments->num_rows() > 0)
						{
							foreach ($payments->result() as $payments_key) {
								# code...
								$payment_date = $payments_key->payment_date;

								$payment_explode = explode('-', $payment_date);
								$payment_year = $payment_explode[0];
								$payment_month = $payment_explode[1];
								$payment_amount = $payments_key->amount_paid;
								$account_payment_id = $payments_key->account_payment_id;

								if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
								{
									$total_arrears -= $payment_amount;
									// if($payment_year >= $current_year)
									// {
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($payment_date)).' </td>
												<td>PAYMENT</td>
												<td></td>
												<td>'.number_format($payment_amount, 2).'</td>
												<td></td>
												<td><a href="'.site_url().'delete-creditor-payment-entry/'.$account_payment_id.'/'.$creditor_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
											</tr> 
										';
									// }
									
									$total_payment_amount += $payment_amount;

								}
							}
						}

						if($bills_query->num_rows() > 0)
						{
							foreach ($bills_query->result() as $supplier) {
								# code...
								$invoice_date_bill = $supplier->invoice_date;
								$invoice_number = $supplier->invoice_number;
								$order_id = $supplier->order_id;
								$invoice_amount = $this->get_total_supplied_invoice($order_id);
								$invoice_explode = explode('-', $invoice_date_bill);

								if(($invoice_date_bill > $invoice_date) &&  ($invoice_amount > 0))
								{
									$total_arrears += $invoice_amount;
									$total_invoice_balance += $invoice_amount;
								
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
												<td>'.strtoupper($invoice_number).'</td>
												<td>Drug Purchases</td>
												<td></td>
												<td>'.number_format($invoice_amount, 2).'</td>
												<td></td>
											</tr> 
										';
									
								}
							}

						}


						if($credit_note_query->num_rows() > 0)
						{
							foreach ($credit_note_query->result() as $credit_note) {
								# code...
								$invoice_date_bill = $credit_note->invoice_date;
								$invoice_number = $credit_note->invoice_number;
								$order_id = $credit_note->order_id;
								$credit_note_amount = $this->get_total_supplied_invoice($order_id);
								$invoice_explode = explode('-', $invoice_date_bill);

								if(($invoice_date_bill > $invoice_date) &&  ($credit_note_amount > 0))
								{
									$total_arrears -= $credit_note_amount;
									$total_payment_amount += $credit_note_amount;
								
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
												<td>'.strtoupper($invoice_number).'</td>
												<td>Credit Note</td>
												<td>'.number_format($credit_note_amount, 2).'</td>
												<td></td>
												<td></td>
											</tr> 
										';
									
								}
							}

						}

						
					}
							$last_date = $invoice_date;
				}
			}	
			else
			{
				//get all loan deductions before date
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_explode = explode('-', $payment_date);
						$payment_year = $payment_explode[0];
						$payment_month = $payment_explode[1];
						$payment_amount = $payments_key->amount_paid;
						$account_payment_id = $payments_key->account_payment_id;

						if(($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>PAYMENT</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td></td>
										<td><a href="'.site_url().'delete-creditor-payment-entry/'.$account_payment_id.'/'.$creditor_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}

				if($bills_query->num_rows() > 0)
				{
					foreach ($bills_query->result() as $supplier) {
						# code...
						$invoice_date_bill = $supplier->invoice_date;
						$invoice_number = $supplier->invoice_number;
						$order_id = $supplier->order_id;
						$invoice_amount = $this->get_total_supplied_invoice($order_id);
						$invoice_explode = explode('-', $invoice_date_bill);

						if(($invoice_amount > 0))
						{
							$total_arrears += $invoice_amount;
							$total_invoice_balance += $invoice_amount;
						
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
										<td>'.strtoupper($invoice_number).'</td>
										<td>Drug Purchases</td>
										<td></td>
										<td>'.number_format($invoice_amount, 2).'</td>
										<td></td>
									</tr> 
								';
							
						}
					}

				}


				if($credit_note_query->num_rows() > 0)
				{
					foreach ($credit_note_query->result() as $credit_note) {
						# code...
						$invoice_date_bill = $credit_note->invoice_date;
						$invoice_number = $credit_note->invoice_number;
						$order_id = $credit_note->order_id;
						$credit_note_amount = $this->get_total_supplied_invoice($order_id);
						$invoice_explode = explode('-', $invoice_date_bill);

						if(($credit_note_amount > 0))
						{
							$total_arrears -= $credit_note_amount;
							$total_payment_amount += $credit_note_amount;
						
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
										<td>'.strtoupper($invoice_number).'</td>
										<td>Credit Note</td>
										<td>'.number_format($credit_note_amount, 2).'</td>
										<td></td>
										<td></td>
									</tr> 
								';
							
						}
					}

				}
				

			}
							
			//display loan
			$result .= 
			'
				<tr>
					<th colspan="3">Total</th>
					<th>'.number_format($total_payment_amount, 2).'</th>
					<th>'.number_format($total_invoice_balance, 2).'</th>
					<td></td>
				</tr> 
			';
			$result .= 
			'
				<tr>
					<th colspan="3"></th>
					<th colspan="2" style="text-align:center;">'.number_format(($total_invoice_balance-$total_payment_amount ), 2).'</th>
				</tr> 
			';



			$response['total_arrears'] = $total_arrears;
			$response['total_invoice_balance'] = $total_invoice_balance;
			$response['invoice_date'] = $invoice_date;
			$response['result'] = $result;
			$response['total_payment_amount'] = $total_payment_amount;

			// var_dump($response); die();

			return $response;
		}

		public function get_creditor_statement_print($creditor_id)
		{

			$creditor_query = $this->creditors_model->get_opening_creditor_balance($creditor_id);
			$bills = $this->get_all_creditor_invoices($creditor_id);

			$bills_query = $this->get_all_suppplier_invoices($creditor_id);
			$credit_note_query = $this->get_all_suppplier_credit_note($creditor_id);
			// var_dump($bills_query); 
			$payments = $this->get_all_payments_creditor($creditor_id);

			$brought_forward_balance = $this->get_balance_brought_forward($creditor_id);

			


			$x=0;

			$bills_result = '';
			$last_date = '';
			$current_year = date('Y');
			$total_invoices = $bills->num_rows();
			$invoices_count = 0;
			$total_invoice_balance = 0;
			$total_arrears = 0;
			$total_payment_amount = 0;
			$result = '';
			$total_pardon_amount = 0;



			if($creditor_query->num_rows() > 0)
			{
				$row = $creditor_query->row();
				$opening_balance = $row->opening_balance;
				$created = $row->created;
				$debit_id = $row->debit_id;
				// var_dump($debit_id); die();
				if($debit_id == 2)
				{
					// this is deni
					$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($created)).' </td>
										<td>OPENING BALANCE</td>
										<td></td>
										<td>'.number_format($opening_balance, 2).'</td>
										<td></td>
									</tr> 
								';
					$total_payment_amount = $opening_balance;

				}
				else
				{
					// this is a prepayment
					$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($created)).' </td>
										<td>OPENING BALANCE</td>
										<td></td>
										<td></td>
										<td>'.number_format($opening_balance, 2).'</td>
									</tr> 
								';
					$total_invoice_balance = $opening_balance;
				}
			}
			

			if($brought_forward_balance == FALSE)
			{
				$result .='';
			}

			else
			{
				$search_title = $this->session->userdata('creditor_search_title');
				if($brought_forward_balance < 0)
				{
					$positive = -$brought_forward_balance;
					$result .= 
								'
									<tr>
										<td colspan=3> B/F</td>
										<td>'.number_format($positive, 2).'</td>
										<td></td>
									</tr> 
								';
					$total_invoice_balance += $positive;

				}
				else
				{
					$result .= 
								'
									<tr>
										<td > B/F</td>
										<td></td>
										<td>'.number_format($brought_forward_balance, 2).'</td>
									</tr> 
								';


					$total_invoice_balance += $brought_forward_balance;
				}
			}


			if($bills->num_rows() > 0)
			{
				foreach ($bills->result() as $key_bills) {
					# code...
					$invoice_date = $key_bills->invoice_date;
					$invoice_number = $key_bills->invoice_number;
					$invoice_amount = $key_bills->invoice_amount;
					$invoice_explode = explode('-', $invoice_date);
					$invoice_year = $invoice_explode[0];
					$invoice_month = $invoice_explode[1];
					$account_invoice_description = $key_bills->account_invoice_description;
					$account_to_id = $key_bills->account_to_id;
					$account_from_id = $key_bills->account_from_id;
					$account_invoice_id = $key_bills->account_invoice_id;
					// var_dump($bills->result()); die();
					$invoices_count++;
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_explode = explode('-', $payment_date);
							$payment_year = $payment_explode[0];
							$payment_month = $payment_explode[1];
							$payment_amount = $payments_key->amount_paid;
							$account_payment_id = $payments_key->account_payment_id;


							if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// var_dump($payment_year); die();
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>PAYMENT</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}


					
					//display disbursment if cheque amount > 0
					if($invoice_amount != 0)
					{
						$total_arrears += $invoice_amount;
						$total_invoice_balance += $invoice_amount;
						$account_name = $this->get_account_name($account_to_id);
						// if($invoice_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($invoice_date)).' </td>
									<td>'.strtoupper($invoice_number).'</td>
									<td>'.$account_invoice_description.'</td>
									<td></td>
									<td>'.number_format($invoice_amount, 2).'</td>
								</tr> 
							';
						// }
					}

					if($bills_query->num_rows() > 0)
					{
						foreach ($bills_query->result() as $supplier) {
							# code...
							$invoice_date_bill = $supplier->invoice_date;
							$invoice_number = $supplier->invoice_number;
							$order_id = $supplier->order_id;
							$invoice_amount = $this->get_total_supplied_invoice($order_id);
							$invoice_explode = explode('-', $invoice_date_bill);

							if(($invoice_date_bill <= $invoice_date) && ($invoice_date_bill > $last_date) && ($invoice_amount > 0))
							{
								$total_arrears += $invoice_amount;
								$total_invoice_balance += $invoice_amount;
							
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
											<td>'.strtoupper($invoice_number).'</td>
											<td>Drug Purchases</td>
											<td></td>
											<td>'.number_format($invoice_amount, 2).'</td>
										</tr> 
									';
								
							}
						}

					}

					if($credit_note_query->num_rows() > 0)
					{
						foreach ($credit_note_query->result() as $credit_note) {
							# code...
							$invoice_date_bill = $credit_note->invoice_date;
							$invoice_number = $credit_note->invoice_number;
							$order_id = $credit_note->order_id;
							$credit_note_amount = $this->get_total_supplied_invoice($order_id);
							$invoice_explode = explode('-', $invoice_date_bill);

							if(($invoice_date_bill <= $invoice_date) && ($invoice_date_bill > $last_date) && ($credit_note_amount > 0))
							{
								$total_arrears -= $credit_note_amount;
								$total_payment_amount += $credit_note_amount;
							
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
											<td>'.strtoupper($invoice_number).'</td>
											<td>Credit Note</td>
											<td>'.number_format($credit_note_amount, 2).'</td>
											<td></td>
										</tr> 
									';
								
							}
						}

					}

							
					//check if there are any more payments
					if($total_invoices == $invoices_count)
					{
						//get all loan deductions before date
						if($payments->num_rows() > 0)
						{
							foreach ($payments->result() as $payments_key) {
								# code...
								$payment_date = $payments_key->payment_date;

								$payment_explode = explode('-', $payment_date);
								$payment_year = $payment_explode[0];
								$payment_month = $payment_explode[1];
								$payment_amount = $payments_key->amount_paid;
								$account_payment_id = $payments_key->account_payment_id;

								if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
								{
									$total_arrears -= $payment_amount;
									// if($payment_year >= $current_year)
									// {
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($payment_date)).' </td>
												<td>PAYMENT</td>
												<td></td>
												<td>'.number_format($payment_amount, 2).'</td>
												<td></td>
											</tr> 
										';
									// }
									
									$total_payment_amount += $payment_amount;

								}
							}
						}

						if($bills_query->num_rows() > 0)
						{
							foreach ($bills_query->result() as $supplier) {
								# code...
								$invoice_date_bill = $supplier->invoice_date;
								$invoice_number = $supplier->invoice_number;
								$order_id = $supplier->order_id;
								$invoice_amount = $this->get_total_supplied_invoice($order_id);
								$invoice_explode = explode('-', $invoice_date_bill);

								if(($invoice_date_bill > $invoice_date) &&  ($invoice_amount > 0))
								{
									$total_arrears += $invoice_amount;
									$total_invoice_balance += $invoice_amount;
								
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
												<td>'.strtoupper($invoice_number).'</td>
												<td>Drug Purchases</td>
												<td></td>
												<td>'.number_format($invoice_amount, 2).'</td>
											</tr> 
										';
									
								}
							}

						}


						if($credit_note_query->num_rows() > 0)
						{
							foreach ($credit_note_query->result() as $credit_note) {
								# code...
								$invoice_date_bill = $credit_note->invoice_date;
								$invoice_number = $credit_note->invoice_number;
								$order_id = $credit_note->order_id;
								$credit_note_amount = $this->get_total_supplied_invoice($order_id);
								$invoice_explode = explode('-', $invoice_date_bill);

								if(($invoice_date_bill > $invoice_date) &&  ($credit_note_amount > 0))
								{
									$total_arrears -= $credit_note_amount;
									$total_payment_amount += $credit_note_amount;
								
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
												<td>'.strtoupper($invoice_number).'</td>
												<td>Credit Note</td>
												<td>'.number_format($credit_note_amount, 2).'</td>
												<td></td>
											</tr> 
										';
									
								}
							}

						}

						
					}
							$last_date = $invoice_date;
				}
			}	
			else
			{
				//get all loan deductions before date
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_explode = explode('-', $payment_date);
						$payment_year = $payment_explode[0];
						$payment_month = $payment_explode[1];
						$payment_amount = $payments_key->amount_paid;
						$account_payment_id = $payments_key->account_payment_id;

						if(($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>PAYMENT</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}

				if($bills_query->num_rows() > 0)
				{
					foreach ($bills_query->result() as $supplier) {
						# code...
						$invoice_date_bill = $supplier->invoice_date;
						$invoice_number = $supplier->invoice_number;
						$order_id = $supplier->order_id;
						$invoice_amount = $this->get_total_supplied_invoice($order_id);
						$invoice_explode = explode('-', $invoice_date_bill);

						if(($invoice_amount > 0))
						{
							$total_arrears += $invoice_amount;
							$total_invoice_balance += $invoice_amount;
						
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
										<td>'.strtoupper($invoice_number).'</td>
										<td>Drug Purchases</td>
										<td></td>
										<td>'.number_format($invoice_amount, 2).'</td>
									</tr> 
								';
							
						}
					}

				}


				if($credit_note_query->num_rows() > 0)
				{
					foreach ($credit_note_query->result() as $credit_note) {
						# code...
						$invoice_date_bill = $credit_note->invoice_date;
						$invoice_number = $credit_note->invoice_number;
						$order_id = $credit_note->order_id;
						$credit_note_amount = $this->get_total_supplied_invoice($order_id);
						$invoice_explode = explode('-', $invoice_date_bill);

						if(($credit_note_amount > 0))
						{
							$total_arrears -= $credit_note_amount;
							$total_payment_amount += $credit_note_amount;
						
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($invoice_date_bill)).' </td>
										<td>'.strtoupper($invoice_number).'</td>
										<td>Credit Note</td>
										<td>'.number_format($credit_note_amount, 2).'</td>
										<td></td>
									</tr> 
								';
							
						}
					}

				}
				

			}
							
			//display loan
			$result .= 
			'
				<tr>
					<th colspan="3">Total</th>
					<th>'.number_format($total_payment_amount, 2).'</th>
					<th>'.number_format($total_invoice_balance, 2).'</th>
				</tr> 
			';
			$result .= 
			'
				<tr>
					<th colspan="3"></th>
					<th colspan="2" style="text-align:center;">'.number_format(($total_invoice_balance-$total_payment_amount ), 2).'</th>
				</tr> 
			';





			$response['total_arrears'] = $total_arrears;
			$response['total_invoice_balance'] = $total_invoice_balance;
			$response['invoice_date'] = $invoice_date;
			$response['result'] = $result;
			$response['total_payment_amount'] = $total_payment_amount;

			// var_dump($response); die();

			return $response;
		}

		public function get_account_name($from_account_id)
		{
			$account_name = '';
			$this->db->select('account_name');
			$this->db->where('account_id = '.$from_account_id);
			$query = $this->db->get('account');
			
			$account_details = $query->row();
			$account_name = $account_details->account_name;
			
			return $account_name;
		}

	public function get_opening_creditor_balance($creditor_id)
	{
		$this->db->select('*'); 
		$this->db->where('creditor_id = '.$creditor_id.'' );
		$query = $this->db->get('creditor');
		
		return $query;
	}

	public function get_opening_provider_balance($provider_id)
	{
		$this->db->select('*'); 
		$this->db->where('provider_id = '.$provider_id.'' );
		$query = $this->db->get('provider_account');
		
		return $query;
	}


	public function get_all_creditors_values()
	{
		$this->db->select('*'); 
		$this->db->where('creditor_id  > 0' );
		$creditor_result = $this->db->get('creditor');
		if($creditor_result->num_rows() > 0)
		{
			foreach ($creditor_result->result() as $key => $creditor) {
				# code...
				$creditor_id = $creditor->creditor_id;

				$this->db->select('*'); 
				$this->db->where('creditor_id = '.$creditor_id.' AND creditor_account_delete = 0' );
				$query = $this->db->get('creditor_account');

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...

						$creditor_account_description = $value->creditor_account_description;
						$creditor_account_amount = $value->creditor_account_amount;
						$creditor_account_date = $value->creditor_account_date;
						$transaction_type_id = $value->transaction_type_id;
						$transaction_code = $value->transaction_code;

						if($transaction_type_id == 2)
						{
							$account = array(
								'account_to_id'=>12,//$this->input->post('account_to_id'),
								'account_from_id'=>$creditor_id,
								'invoice_amount'=>$creditor_account_amount,
								'account_invoice_description'=>$creditor_account_description,
			                    'account_to_type'=>2,//$this->input->post('transaction_type_id'),
			                    'invoice_date'=>$creditor_account_date,
			                    'invoice_number'=>$transaction_code,
			                    'created_by'=>$this->session->userdata('personnel_id'),
			                    'created'=>date('Y-m-d')
								);
							$this->db->insert('account_invoices',$account);
						}
						else if($transaction_type_id == 1)
						{


							$account = array(
								'account_to_id'=>$creditor_id,//$this->input->post('account_to_id'),
								'account_from_id'=>3,
								'amount_paid'=>$creditor_account_amount,//$this->input->post('amount'),
								'account_payment_description'=>$creditor_account_description,//$this->input->post('description'),
			                    'account_to_type'=>2,//$this->input->post('account_to_type'),
			                    'payment_date'=>$creditor_account_date,
			                    'created_by'=>$this->session->userdata('personnel_id'),
			                    'created'=>date('Y-m-d')
								);
							$this->db->insert('account_payments',$account);

						}
						
					}
				}
			}
		}

			
	}

	public function get_cash_collection($payment_month,$payment_year,$provider_id,$patient_type = 0)
	{
		$search = $this->session->userdata('provider_invoice_search');

		if(!empty($search))
		{
			$invoice_search = $search;
		}
		else
		{
			$invoice_search = '';
		}

		if($patient_type == 1)
		{
			$visit_type = ' AND MONTH(visit.visit_date) = "'.$payment_month.'" AND YEAR(visit.visit_date) = "'.$payment_year.'" AND visit.visit_type = 1';
		}
		else
		{
			$visit_type = ' AND MONTH(visit.visit_date) = "'.$payment_month.'" AND YEAR(visit.visit_date) = "'.$payment_year.'" AND visit.visit_type <> 1';
		}
		
		$this->db->from('visit,visit_charge');
		$this->db->select('visit_date,SUM(visit_charge.visit_charge_amount*visit_charge.visit_charge_units) AS total_charged_amount');
		$this->db->where('visit.visit_id = visit_charge.visit_id AND visit.visit_delete = 0 AND visit.personnel_id = '.$provider_id.''.$visit_type);
		$this->db->order_by('YEAR(visit.visit_date),MONTH(visit.visit_date)','ASC');
		$this->db->group_by('YEAR(visit.visit_date),MONTH(visit.visit_date)');
		$query = $this->db->get();
		$total_charged_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_charged_amount = $value->total_charged_amount;
			}
		}
		return $total_charged_amount;
	}

	public function get_all_personnel_providers($table, $where,$order,$order_method,$config,$page)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		//var_dump($query);die();
		return $query;
	}

	public function get_provider_statement($provider_id)
	{
		$creditor_query = $this->creditors_model->get_opening_provider_balance($provider_id);
		$bills = $this->get_all_provider_invoices($provider_id);
		$all_collections = $this->get_all_provider_work_done($provider_id);
		// var_dump($all_collections); die();
		$payments = $this->get_all_payments_provider($provider_id);

		$brought_forward_balance = $this->get_provider_balance_brought_forward($provider_id);

		


		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;


		$opening_balance = 0;

		$opening_date = date('Y-m-d');
		$debit_id = 2;
		// var_dump($creditor_query->num_rows()); die();
		if($creditor_query->num_rows() > 0)
		{
			$row = $creditor_query->row();
			$opening_balance = $row->opening_balance;
			$opening_date = $row->created;
			$debit_id = $row->debit_id;
			// var_dump($debit_id); die();
			if($debit_id == 2)
			{
				// this is deni
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=5>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
									<td></td>
									<td></td>
								</tr> 
							';
				$total_invoice_balance = $opening_balance;

			}
			else
			{
				// this is a prepayment
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td colspan=6>Opening Balance</td>
									<td>'.number_format($opening_balance, 2).'</td>
									<td></td>
								</tr> 
							';
				$total_payment_amount = $opening_balance;
			}
		}
		

		if($brought_forward_balance == FALSE)
		{
			$result .='';
		}

		else
		{
			$search_title = $this->session->userdata('creditor_search_title');
			if($brought_forward_balance < 0)
			{
				$positive = -$brought_forward_balance;
				$result .= 
							'
								<tr>
									<td colspan=4> B/F</td>
									<td>'.number_format($positive, 2).'</td>
									<td></td>
								</tr> 
							';
				$total_invoice_balance += $positive;

			}
			else
			{
				$result .= 
							'
								<tr>
									<td colspan=4> B/F</td>
									<td></td>
									<td>'.number_format($brought_forward_balance, 2).'</td>
								</tr> 
							';


				$total_invoice_balance += $brought_forward_balance;
			}
		}


		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_date = $key_bills->invoice_date;
				$invoice_number = $key_bills->invoice_number;
				$invoice_amount = $key_bills->invoice_amount;
				$invoice_explode = explode('-', $invoice_date);
				$invoice_year = $invoice_explode[0];
				$invoice_month = $invoice_explode[1];
				$account_invoice_description = $key_bills->account_invoice_description;
				$account_to_id = $key_bills->account_to_id;
				$account_from_id = $key_bills->account_from_id;
				$account_invoice_id = $key_bills->account_invoice_id;
				// var_dump($bills->result()); die();
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_explode = explode('-', $payment_date);
						$payment_year = $payment_explode[0];
						$payment_month = $payment_explode[1];
						$payment_amount = $payments_key->amount_paid;
						$account_payment_id = $payments_key->account_payment_id;


						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td></td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td><a href="'.site_url().'delete-provider-payment-entry/'.$account_payment_id.'/'.$provider_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}

				if($all_collections->num_rows() > 0)
				{
					foreach ($all_collections->result() as $collections_key) {
						# code...
						$visit_date = $collections_key->visit_date;
						$payment_explode = explode('-', $visit_date);
						$payment_year = $payment_explode[0];
						$payment_month = $payment_explode[1];
						$amount_charged = $collections_key->total_charged_amount;


						if(($visit_date <= $invoice_date) && ($visit_date > $last_date) && ($amount_charged > 0))
						{
							$total_arrears += $amount_charged;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($visit_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td></td>
										<td>'.number_format($amount_charged, 2).'</td>
										<td></td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_invoice_balance += $amount_charged;

						}
					}
				}
				
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
					$account_name = $this->get_account_name($account_to_id);
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_number.'</td>
								<td>'.$account_invoice_description.'</td>
								<td></td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td><a href="'.site_url().'delete-provider-invoice-entry/'.$account_invoice_id.'/'.$provider_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;

							$payment_explode = explode('-', $payment_date);
							$payment_year = $payment_explode[0];
							$payment_month = $payment_explode[1];
							$payment_amount = $payments_key->amount_paid;
							$account_payment_id = $payments_key->account_payment_id;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td></td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td><a href="'.site_url().'delete-provider-payment-entry/'.$account_payment_id.'/'.$provider_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
						if($all_collections->num_rows() > 0)
					{
						foreach ($all_collections->result() as $collections_key) {
							# code...
							$visit_date = $collections_key->visit_date;
							$payment_explode = explode('-', $visit_date);
							$payment_year = $payment_explode[0];
							$payment_month = $payment_explode[1];
							$visit_charge_amount = $collections_key->visit_charge_amount;
							$amount_charged = $collections_key->total_charged_amount;


							if(($visit_date > $invoice_date) &&  ($amount_charged > 0))
							{
								$total_arrears += $amount_charged;
								// var_dump($payment_year); die();
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($visit_date)).' </td>
											<td>for</td>
											<td></td>
											<td></td>
											<td>'.number_format($amount_charged, 2).'</td>
											<td></td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_invoice_balance += $amount_charged;

							}
						}
					}
					}
					
		

					
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_explode = explode('-', $payment_date);
					$payment_year = $payment_explode[0];
					$payment_month = $payment_explode[1];
					$payment_amount = $payments_key->amount_paid;
					$account_payment_id = $payments_key->account_payment_id;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td colspan="5">Payments </td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td><a href="'.site_url().'delete-provider-payment-entry/'.$account_payment_id.'/'.$provider_id.'" class="btn btn-sm btn-danger fa fa-trash" onclick="return confirm(\'Do you really want delete this entry?\');"></a></td>
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($all_collections->num_rows() > 0)
			{
				foreach ($all_collections->result() as $collections_key) {
					# code...
					$visit_date = $collections_key->visit_date;
					$payment_explode = explode('-', $visit_date);
					$payment_year = $payment_explode[0];
					$payment_month = $payment_explode[1];
					$visit_charge_amount = $collections_key->visit_charge_amount;
					$amount_charged = $collections_key->total_charged_amount;
					$cash_amount = $this->get_cash_collection($payment_month,$payment_year,$provider_id,1);
					$invoice_amount = $this->get_cash_collection($payment_month,$payment_year,$provider_id,0);

					// calculate all lab works done then
					$lab_work_charge = $this->get_all_lab_works($payment_month,$payment_year,$provider_id);

					if(empty($lab_work_charge))
					{
						$lab_work_charge = 0;
					}

					$amount_charged = $amount_charged - $lab_work_charge;

					if($amount_charged > 24000)
					{
						$wht = 0.05 * $amount_charged;
						$amount_value = $amount_charged - $wht;
					}
					else
					{
						$wht = 0;
						$amount_value =  $amount_charged;
					}
					$amount_value = 0.3 * $amount_value;
					if(($amount_value > 0))
					{
						$total_arrears += $amount_value;
						// var_dump($payment_year); die();
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('M Y',strtotime($visit_date)).' Invoice </td>
									<td>'.number_format($cash_amount, 2).'</td>
									<td>'.number_format($invoice_amount, 2).'</td>
									<td>'.number_format($amount_charged+$lab_work_charge, 2).'</td>
									<td>'.number_format($lab_work_charge, 2).'</td>
									<td>'.number_format($wht, 2).'</td>
									<td>'.number_format($amount_value, 2).'</td>
									<td></td>
									<td><a href="'.site_url().'view-doctor-patients/'.$provider_id.'/'.$payment_month.'/'.$payment_year.'" target="_blank" class="btn btn-xs btn-success" >view patients</a></td>
								</tr> 
							';
						// }
						
						$total_invoice_balance += $amount_value;

					}
				}
			}
			

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="6">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<td></td>
			</tr> 
		';
		$result .= 
		'
			<tr>
				<th colspan="6"></th>
				<th colspan="2" style="text-align:center;">'.number_format($total_invoice_balance - $total_payment_amount, 2).'</th>
			</tr> 
		';



		$response['total_arrears'] = $total_arrears;
		$response['total_invoice_balance'] = $total_invoice_balance;
		$response['invoice_date'] = $invoice_date;
		$response['opening_balance'] = $opening_balance;
		$response['opening_date'] = $opening_date;
		$response['debit_id'] = $debit_id;
		$response['result'] = $result;
		$response['total_payment_amount'] = $total_payment_amount;

		// var_dump($response); die();

		return $response;
	}

	public function get_provider_statement_print($provider_id)
	{

		$creditor_query = $this->creditors_model->get_opening_provider_balance($provider_id);
		$bills = $this->get_all_provider_invoices($provider_id);
		// var_dump($bills); 
		$payments = $this->get_all_payments_provider($provider_id);

		$brought_forward_balance = $this->get_provider_balance_brought_forward($provider_id);

		


		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;


		$opening_balance = 0;

		$opening_date = date('Y-m-d');
		$debit_id = 2;
		// var_dump($creditor_query->num_rows()); die();
		if($creditor_query->num_rows() > 0)
		{
			$row = $creditor_query->row();
			$opening_balance = $row->opening_balance;
			$opening_date = $row->created;
			$debit_id = $row->debit_id;
			// var_dump($debit_id); die();
			if($debit_id == 2)
			{
				// this is deni
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td>Opening Balance</td>
									<td></td>
									<td>'.number_format($opening_balance, 2).'</td>
									<td></td>
								</tr> 
							';
				$total_invoice_balance = $opening_balance;

			}
			else
			{
				// this is a prepayment
				$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($created)).' </td>
									<td>Opening Balance</td>
									<td></td>
									<td></td>
									<td>'.number_format($opening_balance, 2).'</td>
								</tr> 
							';
				$total_payment_amount = $opening_balance;
			}
		}
		

		if($brought_forward_balance == FALSE)
		{
			$result .='';
		}

		else
		{
			$search_title = $this->session->userdata('creditor_search_title');
			if($brought_forward_balance < 0)
			{
				$positive = -$brought_forward_balance;
				$result .= 
							'
								<tr>
									<td colspan=3> B/F</td>
									<td>'.number_format($positive, 2).'</td>
								</tr> 
							';
				$total_invoice_balance += $positive;

			}
			else
			{
				$result .= 
							'
								<tr>
									<td > B/F</td>
									<td></td>
									<td>'.number_format($brought_forward_balance, 2).'</td>
								</tr> 
							';


				$total_invoice_balance += $brought_forward_balance;
			}
		}


		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_date = $key_bills->invoice_date;
				$invoice_number = $key_bills->invoice_number;
				$invoice_amount = $key_bills->invoice_amount;
				$invoice_explode = explode('-', $invoice_date);
				$invoice_year = $invoice_explode[0];
				$invoice_month = $invoice_explode[1];
				$account_invoice_description = $key_bills->account_invoice_description;
				$account_to_id = $key_bills->account_to_id;
				$account_from_id = $key_bills->account_from_id;
				$account_invoice_id = $key_bills->account_invoice_id;
				// var_dump($bills->result()); die();
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_explode = explode('-', $payment_date);
						$payment_year = $payment_explode[0];
						$payment_month = $payment_explode[1];
						$payment_amount = $payments_key->amount_paid;
						$account_payment_id = $payments_key->account_payment_id;


						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
					$account_name = $this->get_account_name($account_to_id);
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_number.'</td>
								<td>'.$account_invoice_description.'</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;

							$payment_explode = explode('-', $payment_date);
							$payment_year = $payment_explode[0];
							$payment_month = $payment_explode[1];
							$payment_amount = $payments_key->amount_paid;
							$account_payment_id = $payments_key->account_payment_id;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}

					
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_explode = explode('-', $payment_date);
					$payment_year = $payment_explode[0];
					$payment_month = $payment_explode[1];
					$payment_amount = $payments_key->amount_paid;
					$account_payment_id = $payments_key->account_payment_id;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="3">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
			</tr> 
		';
		$result .= 
		'
			<tr>
				<th colspan="3"></th>
				<th colspan="2" style="text-align:center;">'.number_format($total_invoice_balance - $total_payment_amount, 2).'</th>
			</tr> 
		';



		$response['total_arrears'] = $total_arrears;
		$response['total_invoice_balance'] = $total_invoice_balance;
		$response['invoice_date'] = $invoice_date;
		$response['opening_balance'] = $opening_balance;
		$response['opening_date'] = $opening_date;
		$response['debit_id'] = $debit_id;
		$response['result'] = $result;
		$response['total_payment_amount'] = $total_payment_amount;

		// var_dump($response); die();

		return $response;
	}
public function get_doctor()
	{
		$table = "personnel,personnel_type";
		$where = "personnel.personnel_type_id = personnel_type.personnel_type_id AND personnel_type.personnel_type_name = 'Service Provider'";
		$items = "personnel.personnel_onames, personnel.personnel_fname, personnel.personnel_id";
		$order = "personnel_onames";
		
		$result = $this->database->select_entries_where($table, $where, $items, $order);
		
		return $result;
	}

}
?>