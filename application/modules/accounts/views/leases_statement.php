<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
  $account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}


?>

<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #000 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;

			}
			tr,th,td
			{
				border-color: #000 !important;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
			 <div class="row">
				 <div class="col-xs-12">
						 <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
					 </div>
			 </div>
			 <br>
		 <div class="row">
				 <div class="col-md-12 center-align receipt_bottom_border">
						 <strong>
								 <?php echo $contacts['company_name'];?><br/>
									 P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
									 E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
									 <?php echo $contacts['location'];?>
							 </strong>
					 </div>
			 </div>

		 <div class="row receipt_bottom_border" >
				 <div class="col-md-12 center-align">
						 <h4><strong>TENANTS STATEMENT</strong></h4>
						 <h4><strong>TO</strong> </h4>
						 <h4><strong><?php echo strtoupper($tenant_name)?></strong> </h4>
					 </div>

			 </div>

        <!-- Table row -->
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table class="table table-hover table-bordered">
        		<thead>
        			<tr>
        				<th>Transaction</th>
        				<th>Remarks</th>
                <th>Transaction Date</th>
        				<th>Debit</th>
        				<th>Credit</th>
        				<th>Balance</th>
        			</tr>
        		</thead>
        	  	<tbody>
        	  		<?php echo $tenant_response['result'];?>
        	  	</tbody>
        	</table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
</html>
