<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit <?php echo $title;?></h3>

          <div class="box-tools pull-right">
             <!-- <a href="<?php echo site_url();?>lease-manager/tenants" class="btn btn-info pull-right">Back to tenants</a> -->
          </div>
        </div>
        <div class="box-body">
			<?php
			$error = $this->session->userdata('error_message');
			if(!empty($error))
			{
				?>
				<div class="alert alert-danger">
				<?php echo $error;?>
                </div>
                <?php
				$this->session->unset_userdata('error_message');
			}
            echo form_open("accounts/print-statement", array("class" => "form-horizontal", 'target' => '_blank'));
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Property</label>
                        
                        <div class="col-lg-8">
                           <select id='property_id' name='property_id' class='form-control select2'>
                              <option value=''>None - Please Select a property</option>
                              <?php echo $property_list;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Year</label>
                        
                        <div class="col-lg-8">
                        <input type="text" class="form-control" name="year" placeholder="Year">
                        </div>
                    </div>

                </div>
                 <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Month</label>
                        
                        <div class="col-lg-8">
                           <select name='month_id' class='form-control select2'>
                              <option value=''>None - Please Select a month</option>
                              <option value='01'>Jan</option>
                              <option value='02'>Feb</option>
                              <option value='03'>March</option>
                               <option value='04'>April</option>
                              <option value='05'>May</option>
                              <option value='06'>June</option>
                               <option value='07'>July</option>
                              <option value='08'>August</option>
                              <option value='09'>Septemer</option>
                               <option value='10'>October</option>
                              <option value='11'>November</option>
                              <option value='12'>December</option>
                              
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-8 col-lg-offset-4">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
            echo form_close();
            ?>
        </div>
</div>


        