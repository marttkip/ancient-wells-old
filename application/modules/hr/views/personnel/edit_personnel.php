<?php
//personnel data
$row = $personnel->row();

// var_dump($row) or die();
$personnel_onames = $row->personnel_onames;
$personnel_fname = $row->personnel_fname;
$personnel_dob = $row->personnel_dob;
$personnel_email = $row->personnel_email;
$personnel_phone = $row->personnel_phone;
$personnel_address = $row->personnel_address;
$civil_status_id = $row->civilstatus_id;
$personnel_locality = $row->personnel_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$personnel_username = $row->personnel_username;
$personnel_kin_fname = $row->personnel_kin_fname;
$personnel_kin_onames = $row->personnel_kin_onames;
$personnel_kin_contact = $row->personnel_kin_contact;
$personnel_kin_address = $row->personnel_kin_address;
$kin_relationship_id = $row->kin_relationship_id;
$job_title_idd = $row->job_title_id;
$staff_id = $row->personnel_staff_id;
$bank_branch_id = $row->bank_branch_id;
$bank_id = $row->bank_id;



//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();

if(!empty($validation_error))
{
	$personnel_onames =set_value('personnel_onames');
	$personnel_fname =set_value('personnel_fname');
	$personnel_dob =set_value('personnel_dob');
	$personnel_email =set_value('personnel_email');
	$personnel_phone =set_value('personnel_phone');
	$personnel_address =set_value('personnel_address');
	$civil_status_id =set_value('civil_status_id');
	$personnel_locality =set_value('personnel_locality');
	$title_id =set_value('title_id');
	$gender_id =set_value('gender_id');
	$personnel_username =set_value('personnel_username');
	$personnel_kin_fname =set_value('personnel_kin_fname');
	$personnel_kin_onames =set_value('personnel_kin_onames');
	$personnel_kin_contact =set_value('personnel_kin_contact');
	$personnel_kin_address =set_value('personnel_kin_address');
	$kin_relationship_id =set_value('kin_relationship_id');
	$job_title_id =set_value('job_title_id');
	$staff_id =set_value('staff_id');
	$bank_id2 = set_value('bank_id');
	$bank_branch_id2 = set_value('bank_branch_id');

}
?>
<?php
	$success = $this->session->userdata('success_message');
	$error = $this->session->userdata('error_message');

	if(!empty($success))
	{
		echo '
			<div class="alert alert-success">'.$success.'</div>
		';

		$this->session->unset_userdata('success_message');
	}

	if(!empty($error))
	{
		echo '
			<div class="alert alert-danger">'.$error.'</div>
		';

		$this->session->unset_userdata('error_message');
	}

?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">General Details</a></li>
      <li><a href="#tab_2" data-toggle="tab">Uploads</a></li>
      <li><a href="#tab_3" data-toggle="tab">Account Details</a></li>
      <li><a href="#tab_4" data-toggle="tab">Emergency Contacts</a></li>
      <li><a href="#tab_5" data-toggle="tab">Dependants</a></li>
      <li><a href="#tab_6" data-toggle="tab">Job</a></li>
      <li><a href="#tab_7" data-toggle="tab">Leave</a></li>
      <li><a href="#tab_8" data-toggle="tab">Properties</a></li>
      <li class="pull-right"><a href="<?php echo site_url();?>human-resource/personnel" class="text-muted"><i class="fa fa-arrow-left"></i> Back to personnel</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
		<?php echo $this->load->view('edit/about', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_2">
			<?php echo $this->load->view('edit/uploads', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_3">
       	<?php echo $this->load->view('edit/account', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_4">
       		<?php echo $this->load->view('edit/emergency', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_5">
       	<?php echo $this->load->view('edit/dependants', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_6">
       		<?php echo $this->load->view('edit/jobs', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_7">
       		<?php echo $this->load->view('edit/leave', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
      <div class="tab-pane" id="tab_8">
       		<?php echo $this->load->view('edit/property', '', TRUE);?>
      </div>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>
