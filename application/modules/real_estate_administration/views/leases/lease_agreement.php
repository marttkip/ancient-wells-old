<?php

$all_leases = $this->leases_model->get_lease_detail($lease_id);
// var_dump($all_leases->result()); die();
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date =$leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_email = $leases_row->tenant_email;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$created = $leases_row->created;
		$expense_amount = $leases_row->expense_amount;
		$closing_end_date = $leases_row->closing_end_date;
		$remarks = $leases_row->remarks;
		$closing_water_reading = $leases_row->closing_water_reading;
		$lease_end_date = $leases_row->lease_end_date;
		$vacated_on = $leases_row->vacated_on;
		$notice_date = $leases_row->notice_date;
		$tenant_number = $leases_row->tenant_number;
    $property_owner_name = $leases_row->property_owner_name;

		$rental_unit_id = $leases_row->rental_unit_id;


		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + ".$lease_duration." months");
		// $lease_end_date  = date('jS M Y', strtotime($lease_end_date));

		$total_due = $rent_amount*12;

		$total_paid = 6000;
		$lease_start = $leases_row->lease_start_date;
		// var_dump($leases_row); die();


		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Inactive Lease</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active Lease</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}
		else if($lease_status == 2)
		{
			$status = '<span class="label label-warning">New Lease</span>';
		}
		else if($lease_status == 3)
		{
				$status = '<span class="label label-warning">Lease on Notice</span>';
		}
		else if($lease_status == 4)
		{
				$status = '<span class="label label-danger">Lease has been closed</span>';
		}


	}

	$amount_to_return = $this->accounts_model->get_deposits_paid($lease_id);
	if(empty($amount_to_return))
	{
		$amount_to_return = 0;
	}

	$expenses_payable = $this->accounts_model->get_expenses_payable($lease_id);
	if(empty($expenses_payable))
	{
		$expenses_payable = 0;
	}


	$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
	$total_arrears = $tenants_response['balance'];
	$invoiced = $tenants_response['invoiced'];
	$waived = $tenants_response['waived'];
	$paid = $tenants_response['paid'];

	if($lease_end_date == '0000-00-00')
	{
		$lease_end_date = '';
	}


  $unit_billing_where = 'billing_schedule.billing_schedule_id = property_billing.billing_schedule_id AND property_billing.invoice_type_id = invoice_type.invoice_type_id AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id;
  $unit_billing_table = 'billing_schedule,property_billing,invoice_type';
  $unit_billing_order = 'property_billing.property_billing_id';

  $unit_billing_query = $this->tenants_model->get_tenant_list($unit_billing_table, $unit_billing_where, $unit_billing_order);

  // $v_data['lease_other_documents'] = $this->leases_model->get_document_uploads($lease_id);
  // $v_data['closing_lease_expenses'] = $this->leases_model->get_lease_closing_invoices($lease_id);
  $query_invoice = $unit_billing_query;
  $rent_amount = 0;
  $rent_amount_deposit = 0;
  $rent_amount_agreement = 0;
if($query_invoice->num_rows() > 0)
{
  foreach ($query_invoice->result() as $key => $value) {
    # code...
    $billing_schedule_id = $value->billing_schedule_id;
    $property_billing_id = $value->property_billing_id;
    $invoice_type_id = $value->invoice_type_id;
    $invoice_type_name = $value->invoice_type_name;
    $billing_schedule_name = $value->billing_schedule_name;
    $billing_start_date = $value->start_date;
    $arrears = $value->arrears_bf;
    $billing_amount = $value->billing_amount;
    $initial_water_meter_reading = $value->initial_reading;
    // echo $invoice_type_name;
    if($invoice_type_name == "Rent " OR $invoice_type_name == "Rent")
    {
      $rent_amount = $billing_amount;
    }

    if($invoice_type_name == "Rent Deposit " OR $invoice_type_name == "Rent Deposit")
    {
      $rent_amount_deposit = $billing_amount;
    }

    if($invoice_type_name == "Agreement Fee " OR $invoice_type_name == "Agreement Fee")
    {
      $rent_amount_agreement = $billing_amount;
    }
  }
}


?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
      .title-name
      {
        font-weight: bold;
        border-bottom: 1px dotted #000;
        text-decoration: none;
        text-transform: uppercase;
        text-indent: inherit;
      }
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border" style="float:center;">
                      <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" />
                      <h1 style="text-align:center;">TENANCY AGREEMENT</h1>
                </div>
            </div>
        </div>
        <div class="row">
          <div style="padding:10px">
             <p>This Agreement made on the (Dates) <span class="title-name"><?php echo $lease_start_date?></span> Between Mr. / Mrs./ Miss/ Dr./Prof :- <span class="title-name"><?php echo $tenant_name?></span> IDNumber <span class="title-name"><?php echo $tenant_national_id;?></span> Telephone <span class="title-name"><?php echo $tenant_phone_number?></span>. P.O.BOX: 2379-00100 Nairobi. On Part and Landlord, <span class="title-name"><?php echo $property_owner_name;?></span> or us (as the authorized Agent) on the other part.</p>

             <h3 style="text-decoration:underline;font-style:bold;"> A. NOW IT IS HEAREBY AGREEN AS FOLLOWS:-</h3>
              <p>1.	The Landlord/Agent shall let with vacant possession all that premises in new Donholm-Embaksai East. The tenancy shall commence on the <span class="title-name"><?php echo $lease_start_date?></span> . The lease shall be for a period of <span class="title-name"><?php echo $lease_duration?></span> months.</p>
              <p>2.	Rent of Kshs <span class="title-name"><?php echo number_format($rent_amount,2)?></span> per month is due monthly/quarterly in advance on or before 1st of every month/quarterly and payable at our Bank Account. The same shall be reviewed after every 2 years.</p>
              <p>3.	A deposit equivalent to 1 months’ rent amounting to  <span class="title-name">Kshs. <?php echo number_format($rent_amount_deposit,2)?></span>/= will be paid on or before occupation of the premises.</p>
              <p>4.	Rent received after the 5th of every month will attract a surcharge of 5% of rent or Kshs. 100/= whichever is higher and is payable before the rent is due is receipted. <b>NB: Rent collected by our sales persons in the FIELD WILL ATTRACT A SURCHARGE OF 10%</b></p>
              <p>5.	Rent paid after the 10th of very month and received after the tenant has been visited and <b>the house is locked will attract a charge of kshs. 500/= extra being locking charges thereof.</b></p>
              <p>6.	A legal fee of <span class="title-name">Kshs. <?php echo number_format($rent_amount_agreement,2)?></span>/= is payable upon signing the agreement. (New Tenants)</p>
              <p>7.	Deposit will be refunded when the tenancy has been terminated and after having made provision for all repairs resulting from misuse or neglect to the premises by the tenant.</p>
              <p><b>8.	The Deposit shall not at any time be used or levied for a month of rent during the tenancy period.</b></p>
              <p>9.	The tenancy is on monthly/quarterly basis and can be terminated by either party giving the other party 1 months’ notice or payment in lieu of notice.</p>
              <p>10.	A fee of <b>Kshs. 3000/=</b> will be charged for any notice revoked.</p>
              <p>11.	The tenant shall during the tenancy pay all water electricity bill charges raised during the tenancy.</p>
              <p>12.	The tenant shall during the tenancy keep all fittings, fixtures including doors and windows clean and in good order and condition and after the expiration of the tenancy peacefully and quietly handover to the land landlord/Agent the premises painted abd in good order and condition as the time of entry</p>
              <p>13.	The tenant shall not permit to be done anything which is in the opinion of the Landlord/Agent which would constitute a nuisance to the neighbours</p>
              <p>14.	The tenant shall insure his/her goods against all insurance risks.</p>
              <p>15.	All returned cheques shall attract a penalty of kshs 2000/= and thereafter the landlord or his agent will not accept any more cheques from the drawer.</p>
              <p>16.	Not to make any alterations or additions to the said premises or to drive any nail screw bolt or other fasteners into the wall floors or ceilings without the consent of the Landlord in writing.</p>

             <h3 style="text-decoration:underline;font-style:bold;"> B. THE LANDLORD CONSENT WITH THE TENANT AS FOLLOWS:-</h3>
              <p>1.	Not to use the said for any personal reasons during the term of tenancy.</p>
              <p>2.	To keep all shared places  of the premises adequately clean.</p>
              <p>3.	To keep the structure of the premises in good state of repair which he/she is lable.</p>
              <p>4.	To pay ground rent (if any) and the unimproved dsite value tax and any other servixe charges as when required</p>


             <h3 style="text-decoration:underline;font-style:bold;"> C. IN THE WITNESS WHEREOF:-</h3>
             <p> The parties hereto have herento set their hand this ....... Day of ........</p>
              <br/>
             <p> <b>SIGNED BY THE LANDLORD/AGENT ..............................................................</b> </p>
             <br/>
            <p> <b>SIGNED BY THE TENANT ..............................................................</b></p>
             <br/>
             <p> <b>WITNESS ..............................................................</b></p>


          </div>

        </div>

        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
    </html>
