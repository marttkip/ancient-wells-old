<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $year;
$this_month_item = $month;
$return_date = $return_date;
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
$total_current_invoice = 0;


//if users exist display them
//if users exist display them
if ($query->num_rows() > 0)
{

			$count = $page;
	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>HOUSE NUMBER</th>
				<th>TENANT NAME</th>
				<th>RENT ARREARS</th>
				<th>CURRENT RENT</th>
				<th>INVOICED</th>
				<th>RENT PAID</th>
				<th>BAL C/F</th>
			</tr>
		</thead>
		  <tbody>

	';

	foreach ($query->result() as $leases_row)
	{
		$rental_unit_name = $leases_row->rental_unit_name;
		$rental_unit_id = $leases_row->rental_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;

		$todays_month = $month;//$this->session->userdata('month');
		$todays_year = $year;//$this->session->userdata('year');
		$return_date = '01';//$this->session->userdata('return_date');
		// var_dump($todays_month);die();

		if($todays_month < 9)
		{
			$todays_month = $todays_month;
		}
		$lease_date = $todays_year.'-'.$todays_month.'-'.$return_date;
		// var_dump($lease_date); die();

		$lease_query = $this->reports_model->get_statement_leases($rental_unit_id,$lease_date,$todays_year,$todays_month);

		$total_paid_amount = 0;
		if($lease_query->num_rows() > 0)
		{

			$value = $lease_query->row() ;
			// foreach ($lease_query->result() as $key => $value) {

				$lease_id = $value->lease_id;
				// $tenant_unit_id = $value->tenant_unit_id;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_name = $value->tenant_name;
				$tenant_phone_number = $value->tenant_phone_number;
				$rent_payable = $value->billing_amount;

				$parent_invoice_date = date('Y-m-d', strtotime($todays_year.'-'.$todays_month.'-'.$return_date));
				$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month'));

				// $tenants_response = $this->accounts_model->get_rent_and_service_charge($lease_id);
				$total_pardon_amount = 0;//$tenants_response['total_pardon_amount'];


				$rent_invoice_amount = 0;//$this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,1);
				$rent_paid_amount = 0;//$this->accounts_model->get_paid_tenants_brought_forward($lease_id,$previous_invoice_date,1);
				$total_rent_brought_forward = $rent_invoice_amount - $rent_paid_amount -$total_pardon_amount;

				$rent_current_invoice =  0;//$this->accounts_model->get_invoice_tenants_current_month_forward($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);
				$rent_current_payment =  0;//$this->accounts_model->get_today_tenants_paid_current_month_forward($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);

				$total_variance = $rent_current_invoice - $rent_current_payment;
				$total_balance = $total_variance + $total_rent_brought_forward;


				$count++;
				$result .=
							'	<tr>
									<td>'.$count.'</td>
									<td>'.$rental_unit_name.'</td>
									<td>'.$tenant_name.'</td>
									<td>'.number_format($total_rent_brought_forward,2).'</td>
									<td>'.number_format($rent_payable,2).'</td>
									<td>'.number_format($rent_current_invoice,2).'</td>
									<td>'.number_format($rent_current_payment,2).'</td>
									<td>'.number_format($total_balance,0).'</td>
								</tr>
							';

				$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
				$total_arreas_end = $total_arreas_end + $total_rent_brought_forward;
				$total_payable_end = $total_payable_end + $rent_current_payment;
				$total_current_invoice += $rent_current_invoice;
				$total_rent_end = $total_rent_end + $rent_payable;



		}
		else{

			$result .=
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$rental_unit_name.'</td>
							<td>Vacant House</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
						</tr>
					';
		}








	}


	$result .='<tr>
					<td> </td>
					<td></td>
					<td><strong>Totals</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_current_invoice,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items for this month";
}



$property_id = $this->session->userdata('property_id');


//  GET ALL OUT FLOWS OF THE MONTH
$outflows = $this->reports_model->get_property_month_outflows($property_id,$this_month_item,$this_year_item);
$total_outflows = 0;
$x = 0;


// var_dump($outflows); die();
if($outflows->num_rows() > 0)
{
	$outflow = '';

	foreach ($outflows->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$account_invoice_description = $key->account_invoice_description;
		$invoice_amount = $key->invoice_amount;
		$x++;
		$outflow .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$account_invoice_description.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$total_outflows = $total_outflows + $invoice_amount;
	}




	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$outflow ='<tr><td colspan=3>No expense added</td></tr>';
}


$landlord_outflows = $this->reports_model->get_property_month_landlord_outflows($property_id,$this_month_item,$this_year_item);
$total_landlord_outflows = 0;
$x = 0;

$landlord_outflow = '';
// var_dump($landlord_outflows); die();
if($landlord_outflows->num_rows() > 0)
{


	foreach ($landlord_outflows->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$account_invoice_description = $key->account_invoice_description;
		$invoice_amount = $key->invoice_amount;
		$x++;
		$landlord_outflow .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$account_invoice_description.'</td>
						<td>KES. ('.number_format($invoice_amount,2).')</td>
					</tr>';
		$total_landlord_outflows = $total_landlord_outflows + $invoice_amount;
	}




	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$landlord_outflow ='<tr><td colspan=3>No landlord payments</td></tr>';
}
// var_dump($total_outflows); die();

$income = $this->reports_model->get_property_month_income($property_id,$this_month_item,$this_year_item);
$income_amount = 0;
$x = 0;


// var_dump($income); die();
if($income->num_rows() > 0)
{
	$incomes = '';

	foreach ($income->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$account_invoice_description = $key->account_invoice_description;
		$invoice_amount = $key->invoice_amount;
		$x++;
		$incomes .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$account_invoice_description.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$income_amount = $income_amount + $invoice_amount;
	}




	// $incomes .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$incomes ='<tr><td colspan=3>No other incomes recorded</td></tr>';
}

// $percent = $this->reports_model->get_manager_percent($property_id);
// $commission = $total_payable_end * ($percent/100);
// $x++;
// $outflow .='<tr>
// 						<td>'.$x.'</td>
// 						<td colspan=2>Agent Commission '.$percent.'% </td>
// 						<td>KES. '.number_format($commission,2).'</td>
// 					</tr>';
// $total_outflows += $commission;
//
//
// $outflow .='<tr>
// 				<td colspan=2>Total<td>
// 				<td>KES. '.number_format($total_outflows,2).'</td>
// 		</tr>';



$leases = $this->reports_model->get_property_month_leases($property_id,$this_month_item,$this_year_item);
$total_leases = 0;
$x = 0;
$total_deposit_refund = 0;
if($leases->num_rows() > 0)
{
	$closed_leases = '';

	foreach ($leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$remarks.'</td>
								<td>KES. '.number_format($expense_amount,2).'</td>
								<td>KES. '.number_format($deposit_amount - $expense_amount,2).'</td>
							</tr>';

		$total_leases = $total_leases + $expense_amount;
		$total_deposit_refund = $total_deposit_refund + ($deposit_amount - $expense_amount);
	}
	$closed_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_leases,2).'</td>
						<td>KES. '.number_format($total_deposit_refund,2).'</td>
				</tr>';
	// $closed_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_leases ,2).' </strong> </li>';
}else
{
	$closed_leases ='<tr><td colspan=3>No closed leases</td></tr>';
}



$new_leases = $this->reports_model->get_property_month_new_leases($property_id,$this_month_item,$this_year_item);
$total_new_lease = 0;
$total_deposit = 0;
$x = 0;
if($new_leases->num_rows() > 0)
{
	$closed_new_leases = '';

	foreach ($new_leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);

		$x++;
		$closed_new_leases .='<tr>
						<td>'.$x.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.number_format($deposit_amount,2).'</td>
					</tr>';
		// if($expense_amount > 0)
		// {

		// }
		$total_new_lease = $total_new_lease + $deposit_amount;
	}

	$closed_new_leases .='<tr>
					<td colspan="2"></td>
					<td><b>TOTAL</b></td>
					<td>'.number_format($total_new_lease,2).'</td>
				</tr>';

	// $closed_new_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_new_lease ,2).' </strong> </li>';
}else
{
	$closed_new_leases ='<tr><td colspan=4>No expense added</td></tr>';
}
?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border">
                    <table class="table table-condensed">
                        <tr>
                            <th><h2><?php echo $contacts['company_name'];?> </h2></th>
                            <th class="align-right">
                                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="row" >
       		<div class="col-xs-12" style="border-top:1px #000 solid;">
       			<h4>Key Areas to Note:</h4>
       			<div class="row">
       				<div class="col-md-12">
	       				<h5>Tenant Deposits</h5>
	       				<table class="table table-hover table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Rental Unit</th>
										  <th style="width:25%">Tenant</th>
											<th style="width:25%">Deposit Amount</th>

										</tr>
									</thead>
									<tbody>
			       						<?php echo $closed_new_leases;?>
									</tbody>
								</table>

	       			</div>


       			</div>
       			<div class="row">
	       			<div class="col-md-12">
	       				<h5>Expenses</h5>
	       				<table class="table table-hover table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $outflow;?>
									</tbody>
								</table>



								<h5>Incomes</h5>
	       				<table class="table table-hover table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $incomes;?>
									</tbody>
								</table>


								<h5>Landloard Receipts</h5>
	       				<table class="table table-hover table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<tr><td colspan=4>No landlord receipts</td></tr>
									</tbody>
								</table>


								<h5>Landlord payments</h5>
	       				<table class="table table-hover table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $landlord_outflow;?>
									</tbody>
								</table>
	       			</div>

	       		</div>

						<div class="row">
							<div class="col-md-6">
							</div>
	       			<div class="col-md-6">
								<h4>Total Rent Received</h4>
								<table class="table table-hover table-bordered ">
									<thead>

									<tbody>
												<tr>
														<td>Total Gross Payable</td>
														<td><?php echo number_format($total_payable_end,2);?></td>
												</tr>
												<tr>
														<td>Add Other Deposits</td>
														<td><?php echo number_format($total_new_lease,2);?></td>
												</tr>
												<tr>
														<td>Add Refunds</td>
														<td>0.00</td>
												</tr>
												<tr>
														<td>Less Landlord Advance</td>
														<td><?php echo number_format($total_landlord_outflows,2);?></td>
												</tr>
												<tr>
														<td>Less Other Payments</td>
														<td><?php echo number_format($income_amount,2);?></td>
												</tr>
												<tr>
														<td>OUTSTANDING PAYABLE</td>
														<td><?php echo number_format($total_payable_end+$total_new_lease - $total_landlord_outflows + $income_amount);?></td>
												</tr>
												<tr>
														<td><b>NET AMOUNT PAYABLE</b></td>
														<td></td>
												</tr>
									</tbody>
								</table>
							</div>
						</div>
       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
    </html>
