<?php

class Home_owners_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	
	/*
	*	Retrieve all home_owners
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_home_owners($table, $where, $per_page, $page, $order = 'home_owners.home_owner_name', $order_method = 'ASC')
	{
		//retrieve all home_owners
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		// $this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Retrieve all administrators
	*
	*/
	public function get_active_home_owners()
	{
		$this->db->from('personnel');
		$this->db->select('*');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Retrieve all front end home_owners
	*
	*/
	public function get_all_front_end_home_owners()
	{
		$this->db->from('home_owners');
		$this->db->select('*');
		$this->db->where('home_owner_level_id = 2');
		$query = $this->db->get();
		
		return $query;
	}
	

	public function get_all_countries()
	{
		//retrieve all home_owners
		$query = $this->db->get('country');
		
		return $query;
	}
	
	/*
	*	Add a new home_owner to the database
	*
	*/
	public function add_home_owner()
	{
		$data = array(
				'home_owner_name'=>ucwords(strtolower($this->input->post('home_owner_name'))),
				'home_owner_email'=>$this->input->post('home_owner_email'),
				'home_owner_number'=>$this->create_home_owner_number(),
				'home_owner_national_id'=>$this->input->post('home_owner_national_id'),
				'home_owner_phone_number'=>$this->input->post('home_owner_phone_number'),
				'created'=>date('Y-m-d H:i:s'),
				'home_owner_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_home_owner_to_unit($rental_unit_id)
	{
		$this->db->where('home_owner_unit_status = 1 AND rental_unit_id = '.$rental_unit_id.'');
		$this->db->from('home_owner_unit');
		$this->db->select('*');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$home_owner_unit_id = $key->home_owner_unit_id;
				$home_owner_unit_status = $key->home_owner_unit_status;
					// update the details the status to 1 
				$update_array = array('home_owner_unit_status'=>0);
				$this->db->where('home_owner_unit_id = '.$home_owner_unit_id);
				$this->db->update('home_owner_unit',$update_array);
			}
			$insert_array = array(
							'home_owner_id'=>$this->input->post('home_owner_id'),
							'rental_unit_id'=>$rental_unit_id,
							'created'=>date('Y-m-d'),
							'created_by'=>$this->session->userdata('personnel_id'),
							'home_owner_unit_status'=>1,
							);
			$this->db->insert('home_owner_unit',$insert_array);
			return TRUE;
		}
		else
		{
			// create the home_owner unit number
			$insert_array = array(
							'home_owner_id'=>$this->input->post('home_owner_id'),
							'rental_unit_id'=>$rental_unit_id,
							'created'=>date('Y-m-d'),
							'created_by'=>$this->session->userdata('personnel_id'),
							'home_owner_unit_status'=>1,
							);
			$this->db->insert('home_owner_unit',$insert_array);
			$home_owner_unit_id = $this->db->insert_id();

			return TRUE;
		}
	}
	
	/*
	*	Add a new front end home_owner to the database
	*
	*/
	public function add_frontend_home_owner()
	{
		$data = array(
				'home_owner_name'=>ucwords(strtolower($this->input->post('home_owner_name'))),
				'home_owner_email'=>$this->input->post('home_owner_email'),
				'home_owner_national_id'=>$this->input->post('home_owner_national_id'),
				'home_owner_password'=>md5(123456),
				'home_owner_phone_number'=>$this->input->post('home_owner_phone_number'),
				'created'=>date('Y-m-d H:i:s'),
				'home_owner_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id'),
			);
			
		if($this->db->insert('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Edit an existing home_owner
	*	@param int $home_owner_id
	*
	*/
	public function edit_home_owner($home_owner_id)
	{
		$data = array(
				'home_owner_name'=>ucwords(strtolower($this->input->post('home_owner_name'))),
				'home_owner_email'=>$this->input->post('home_owner_email'),
				'home_owner_national_id'=>$this->input->post('home_owner_national_id'),
				'home_owner_phone_number'=>$this->input->post('home_owner_phone_number'),
				'home_owner_status'=>1,
				'modified_by'=>$this->session->userdata('personnel_id'),
			);
		
		
		$this->db->where('home_owner_id', $home_owner_id);
		
		if($this->db->update('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Edit an existing home_owner
	*	@param int $home_owner_id
	*
	*/
	public function edit_frontend_home_owner($home_owner_id)
	{
		$data = array(
				'home_owner_name'=>ucwords(strtolower($this->input->post('home_owner_name'))),
				'other_names'=>ucwords(strtolower($this->input->post('last_name'))),
				'phone'=>$this->input->post('phone')
			);
		
		//check if home_owner wants to update their password
		$pwd_update = $this->input->post('admin_home_owner');
		if(!empty($pwd_update))
		{
			if($this->input->post('old_password') == md5($this->input->post('current_password')))
			{
				$data['password'] = md5($this->input->post('new_password'));
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'The current password entered does not match your password. Please try again');
			}
		}
		
		$this->db->where('home_owner_id', $home_owner_id);
		
		if($this->db->update('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Edit an existing home_owner's password
	*	@param int $home_owner_id
	*
	*/
	public function edit_password($home_owner_id)
	{
		if($this->input->post('slug') == md5($this->input->post('current_password')))
		{
			if($this->input->post('new_password') == $this->input->post('confirm_password'))
			{
				$data['password'] = md5($this->input->post('new_password'));
		
				$this->db->where('home_owner_id', $home_owner_id);
				
				if($this->db->update('home_owners', $data))
				{
					$return['result'] = TRUE;
				}
				else{
					$return['result'] = FALSE;
					$return['message'] = 'Oops something went wrong and your password could not be updated. Please try again';
				}
			}
			else{
					$return['result'] = FALSE;
					$return['message'] = 'New Password and Confirm Password don\'t match';
			}
		}
		
		else
		{
			$return['result'] = FALSE;
			$return['message'] = 'You current password is not correct. Please try again';
		}
		
		return $return;
	}
	
	/*
	*	Retrieve a single home_owner
	*	@param int $home_owner_id
	*
	*/
	public function get_home_owner($home_owner_id)
	{
		//retrieve all home_owners
		$this->db->from('home_owners');
		$this->db->select('*');
		$this->db->where('home_owner_id = '.$home_owner_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Retrieve a single home_owner by their email
	*	@param int $email
	*
	*/
	public function get_home_owner_by_email($email)
	{
		//retrieve all home_owners
		$this->db->from('home_owners');
		$this->db->select('*');
		$this->db->where('email = \''.$email.'\'');
		$query = $this->db->get();
		
		return $query;
	}

	
	
	/*
	*	Delete an existing home_owner
	*	@param int $home_owner_id
	*
	*/
	public function delete_home_owner($home_owner_id)
	{
		if($this->db->delete('home_owners', array('home_owner_id' => $home_owner_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated home_owner
	*	@param int $home_owner_id
	*
	*/
	public function activate_home_owner($home_owner_id)
	{
		$data = array(
				'home_owner_status' => 1
			);
		$this->db->where('home_owner_id', $home_owner_id);
		
		if($this->db->update('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an home_owner_status home_owner
	*	@param int $home_owner_id
	*
	*/
	public function deactivate_home_owner($home_owner_id)
	{
		$data = array(
				'home_owner_status' => 0
			);
		$this->db->where('home_owner_id', $home_owner_id);
		
		if($this->db->update('home_owners', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Reset a home_owner's password
	*	@param string $email
	*
	*/
	public function reset_password($email)
	{
		//reset password
		$result = md5(date("Y-m-d H:i:s"));
		$pwd2 = substr($result, 0, 6);
		$pwd = md5($pwd2);
		
		$data = array(
				'password' => $pwd
			);
		$this->db->where('email', $email);
		
		if($this->db->update('home_owners', $data))
		{
			//email the password to the home_owner
			$home_owner_details = $this->home_owners_model->get_home_owner_by_email($email);
			
			$home_owner = $home_owner_details->row();
			$home_owner_name = $home_owner->home_owner_name;
			
			//email data
			$receiver['email'] = $this->input->post('email');
			$sender['name'] = 'Fad Shoppe';
			$sender['email'] = 'info@fadshoppe.com';
			$message['subject'] = 'You requested a password change';
			$message['text'] = 'Hi '.$home_owner_name.'. Your new password is '.$pwd;
			
			//send the home_owner their new password
			if($this->email_model->send_mail($receiver, $sender, $message))
			{
				return TRUE;
			}
			
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", strtolower($field_name));
		
		return $web_name;
	}
	public function change_password()
	{
		
		$data = array(
				'personnel_password' => md5($this->input->post('new_password'))
			);
		$this->db->where('personnel_password = "'.md5($this->input->post('current_password')).'" AND personnel_id ='.$this->session->userdata('personnel_id'));
		
		if($this->db->update('personnel', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_tenancy_details($home_owner_id,$rental_unit_id)
	{
		$this->db->from('home_owner_unit');
		$this->db->select('*');
		$this->db->where('home_owner_id = '.$home_owner_id.' AND rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get();
		
		return $query;
	}

	public function check_for_account($rental_unit_id)
	{

		$this->db->from('home_owner_unit');
		$this->db->select('*');
		$this->db->where('home_owner_unit_status = 1 AND rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_home_owner_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');
		
		return $query;
	}
	public function home_owners_import_template()
	{
		$this->load->library('Excel');
		
		$title = 'home_owners Import Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'home_owner Name';
		$report[$row_count][1] = 'National ID';
		$report[$row_count][2] = 'Phone Number';
		$report[$row_count][3] = 'Email Address';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
	public function import_csv_home_owners($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$home_owners_array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			
			$response2 = $this->sort_home_owner_data($home_owners_array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	//sort the projects imported into the db
	public function sort_home_owner_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);
		
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 4))
		{

			$items['created_by'] = $this->session->userdata('personnel_id');

			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$home_owner_number = $this->create_home_owner_number();
				$home_owner_name = $items['home_owner_name'] = $array[$r][0];
				$items['home_owner_name'] = $array[$r][0];
				$items['home_owner_national_id'] = $array[$r][1];
				$items['home_owner_phone_number'] = $array[$r][2];
				$home_owner_phone_number = $items['home_owner_phone_number'] = $array[$r][2];
				$items['home_owner_email']=$array[$r][3];
				$items['home_owner_status'] = 1;
				$items['created'] = date('Y-m-d H-i-s');
				$items['home_owner_number'] = $home_owner_number;
				$comment ='';
		
				//check if the project name already exists
				if($this->check_home_owner_exist($home_owner_name,$home_owner_phone_number))
				{

				}
				else
				{
					$this->db->insert('home_owners', $items);
				}
				
				
			}
			
			$response = 'success';
			
			$return['response'] = $response;
			$return['check'] = TRUE;
		}
		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}
		
		return $return;
	}

	public function create_home_owner_number()
	{
		//select product code
		$this->db->from('home_owners');
		$this->db->where("home_owner_number LIKE '".$this->session->userdata('branch_code')."".date('y')."-%'");
		$this->db->select('MAX(home_owner_number) AS number');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			
			if($number == 1){
				$number = "".$this->session->userdata('branch_code')."".date('y')."-001";
			}
		}
		else{//start generating receipt numbers
			$number = "".$this->session->userdata('branch_code')."".date('y')."-001";
		}
		
		return $number;
	}

	public function check_home_owner_exist($home_owner_name,$home_owner_phone_number)
	{
		$this->db->where(array ('home_owner_name'=> $home_owner_name));
		
		$query = $this->db->get('home_owners');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$home_owner_id = $key->home_owner_id;
			}
			return $home_owner_id;
		}
		
		else
		{
			return FALSE;
		}
	}
}
?>