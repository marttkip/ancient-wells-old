<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Tickets extends admin {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
		$this->load->model('ticket_management/tickets_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../invoices/');
		$this->statement_path = realpath(APPPATH . '../statements/');
	}

  public function resolved_tickets()
  {
    $where = 'v_tenants_tickets.ticket_status = 2';
		$table = 'v_tenants_tickets';

		$accounts_search = $this->session->userdata('search_tickets');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;
		}
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'ticket-management/resolved-tickets';
		$config['total_rows'] = $this->tickets_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->tickets_model->get_all_tenants_tickets($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$data['title'] = 'Resolved Tickets';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('tickets/tickets_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
  }


  public function unresolved_tickets()
  {
		$where = 'v_tenants_tickets.ticket_status <> 2';
		$table = 'v_tenants_tickets';

		$accounts_search = $this->session->userdata('search_tickets');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;
		}
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'ticket-management/unresolved-tickets';
		$config['total_rows'] = $this->tickets_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
				$v_data["links"] = $this->pagination->create_links();
		$query = $this->tickets_model->get_all_tenants_tickets($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$data['title'] = 'Unresolved Tickets';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('tickets/tickets_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
  }

  public function ticket_timeline($ticket_id)
  {

    $where = 'v_tenants_tickets.ticket_id ='.$ticket_id;
    $table = 'v_tenants_tickets';
    $this->db->where($where);
    $query = $this->db->get($table);
    $data['title'] = ' Tickets';
    $v_data['title'] = $data['title'];
    $v_data['query'] = $query;
    $data['content'] = $this->load->view('tickets/ticket_timeline', $v_data, true);

    $this->load->view('admin/templates/general_page', $data);

  }

	public function add_ticket_message($ticket_id ,$lease_number,$lease_id)
	{
		$this->form_validation->set_rules('message', 'message', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{

				$array_add['chat_message'] = $this->input->post('message');
				$array_add['sender_id'] = 1;//$this->session->userdata('personnel_id');
				$array_add['lease_id'] = $lease_id;
				$array_add['account_id'] = $lease_number;
				$array_add['chat_status'] = 0;
				$array_add['chat_type'] = 2;
				$array_add['ticket_id'] = $ticket_id;
				$array_add['created'] = date('Y-m-d H:i:s');
				$this->db->insert('chat',$array_add);

				// $this->session->set_userdata("success_message", 'Invoice successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function close_ticket($ticket_id)
	{

		$this->form_validation->set_rules('general_observations', 'message', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{
				$array_add['general_observations'] = $this->input->post('general_observations');
				$array_add['closed_by'] = $this->session->userdata('personnel_id');
				$array_add['date_closed'] = date('Y-m-d H:i:s');
				$array_add['ticket_status_id'] = 2;
				$array_add['ticket_status'] = 2;
				$this->db->where('ticket_id',$ticket_id);
				if($this->db->update('ticket',$array_add))
				{

					$where = 'v_tenants_tickets.ticket_id ='.$ticket_id;
					$table = 'v_tenants_tickets';
					$this->db->where($where);
					$query = $this->db->get($table);
					$leases_row = $query->row();

				  $tenant_unit_id = $leases_row->tenant_unit_id;
				  $rental_unit_id = $leases_row->unit_id;
				  $tenant_name = $leases_row->tenant_name;
				  $tenant_phone_number = $leases_row->tenant_phone_number;
				  $ticket_created = $leases_row->ticket_created;
				  $ticket_description = $leases_row->ticket_description;
				  $ticket_status = $leases_row->ticket_status;
				  $ticket_number = $leases_row->ticket_number;
				  $account_id = $leases_row->account_id;
				  $ticket_id = $leases_row->ticket_id;

					// $tenant_phone_number = '0721625777';
					// $tenant_phone_number = '0734808007';
					if(!empty($tenant_phone_number))
					{
						 $explode = explode(' ',$tenant_name);
						 $first_name = $explode[0];
							$message = 'Dear '.$first_name.', This is to let you know that your issue under ticket no. '.$ticket_number.' created on '.$ticket_created.' has been marked as completed. For help please call 0702777717. Allan & Bradley Co. LTD';
							$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);
					}

				}
				redirect('ticket-management/resolved-tickets');
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			redirect('ticket-timeline/'.$ticket_id);
		}


	}

	public function search_tickets()
	{
		$ticket_no = $this->input->post('ticket_no');
		$tenant_name = $this->input->post('tenant_name');
		$contract_no = $this->input->post('contract_no');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($ticket_no))
		{
			// $this->db->where('ticket_no', $ticket_no);
			// $query = $this->db->get('property');

			$ticket_no = ' AND v_tenants_tickets.ticket_number = "'.$ticket_no.'" ';


		}

		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name.' ';
			$tenant_name = ' AND v_tenants_tickets.tenant_name LIKE \'%'.$tenant_name.'%\'';


		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($contract_no))
		{
			$search_title .= $contract_no.' ';
			$contract_no = ' AND v_tenants_tickets.lease_number = '.$contract_no.'';


		}
		else
		{
			$contract_no = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND v_tenants_tickets.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';

		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $ticket_no.$tenant_name.$rental_unit_name.$contract_no;

		$this->session->set_userdata('search_tickets', $search);
		// $this->session->set_userdata('accounts_search_title', $search_title);
		redirect('ticket-management/resolved-tickets');
	}
	public function close_search_tickets()
	{
		$this->session->unset_userdata('search_tickets');
		// $this->session->set_userdata('accounts_search_title', $search_title);
		redirect('ticket-management/resolved-tickets');
	}
	public function add_ticket()
	{
		$this->form_validation->set_rules('lease_id', 'Lease Number', 'required|xss_clean|required');
		$this->form_validation->set_rules('ticket_description', 'Ticket Description', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->tickets_model->add_ticket())
			{
				$this->session->unset_userdata('tenants_error_message');
				$this->session->set_userdata('success_message', 'Tenant has been successfully added');

				$where = 'v_tenants_tickets.ticket_id ='.$ticket_id;
				$table = 'v_tenants_tickets';
				$this->db->where($where);
				$query = $this->db->get($table);
				$leases_row = $query->row();

				$tenant_unit_id = $leases_row->tenant_unit_id;
				$rental_unit_id = $leases_row->unit_id;
				$tenant_name = $leases_row->tenant_name;
				$tenant_phone_number = $leases_row->tenant_phone_number;
				$ticket_created = $leases_row->ticket_created;
				$ticket_description = $leases_row->ticket_description;
				$ticket_status = $leases_row->ticket_status;
				$ticket_number = $leases_row->ticket_number;
				$account_id = $leases_row->account_id;
				$ticket_id = $leases_row->ticket_id;

				// $tenant_phone_number = '0721625777';
				// $tenant_phone_number = '0734808007';
				if(!empty($tenant_phone_number))
				{
					 $explode = explode(' ',$tenant_name);
					 $first_name = $explode[0];
						$message = 'Dear '.$first_name.', This is to let you know that your issue has been raised on ticket. '.$ticket_number.'. We will be in touch on any progress concerning your issue. For help please call 0702777717. Allan & Bradley Co. LTD';
						$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);
				}


				redirect('ticket-management/unresolved-tickets');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');

			}
		}
		$v_data['title'] = $data['title'] = 'Add Ticket ';
		$data['content'] = $this->load->view('tickets/add_ticket', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>
