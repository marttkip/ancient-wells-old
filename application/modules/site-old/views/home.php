		<!-- START BANNER -->
		<?php echo $this->load->view('home/slider', '', TRUE); ?>
		<!-- END BANNER -->
        
        <!-- START FEATURES SECTION -->
		<?php echo $this->load->view('home/about', '', TRUE); ?>
        <!-- END FEATURES SECTION -->
        
        <!-- START SCREENSHOTS SECTION -->
		<?php echo $this->load->view('home/services', '', TRUE); ?>
        <!-- END SCREENSHOTS SECTION--> 
		
		<?php //echo $this->load->view('home/trainings', '', TRUE); ?>

        <!-- START REVIEW SECTION -->
		<?php echo $this->load->view('home/testimonials', '', TRUE); ?>
        <!-- END REVIEW SECTION -->
        
		<?php echo $this->load->view('home/mobile', '', TRUE); ?>
		
		<?php echo $this->load->view('home/installs', '', TRUE); ?>
        