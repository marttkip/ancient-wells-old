<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// require_once "./application/modules/accounts/controllers/accounts.php";

class Reporting  extends MX_Controller
{
	var $attachments_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reporting/reporting_model');
    $this->load->model('admin/dashboard_model');
      $this->load->model('site/site_model');
      $this->load->model('admin/email_model');
		$this->attachments_path = realpath(APPPATH . '../assets/attachments');
	}

	function daily_report()
	{
		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));
		$branch = $this->config->item('branch_name');
		$message['subject'] = $subject =  $visit_date.' REPORT';
		// var_dump($date_tomorrow); die();
    $v_data['transaction_date'] = $date_tomorrow;
		$text =  $this->load->view('transactions_report', $v_data,true);

		$message['text'] =$text;
		$contacts = $this->site_model->get_contacts();
		$sender_email =$this->config->item('sender_email');//$contacts['email'];
		$shopping = "";
		$from = $sender_email;

		$button = '';
		$sender['email']= $sender_email;
		$sender['name'] = $contacts['company_name'];
		$receiver['name'] = $subject;
		// $payslip = $title;

		$sender_email = $sender_email;
		$tenant_email = $this->config->item('recepients_email');

		$email_array = explode('/', $tenant_email);
		$total_rows_email = count($email_array);
    	// echo $tenant_email; die();
		for($x = 0; $x < $total_rows_email; $x++)
		{
			$receiver['email'] = $email_tenant = $email_array[$x];
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, NULL);
		}

    echo "<script>window.close();</script>";

 }
}

?>
