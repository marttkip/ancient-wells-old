/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : allan

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-02-07 18:08:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `chat`
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `chat_id` int(50) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `chat_message` text,
  `chat_type` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `chat_status` int(11) DEFAULT '0',
  `lease_id` int(50) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of chat
-- ----------------------------

-- ----------------------------
-- Table structure for `credit_note_item`
-- ----------------------------
DROP TABLE IF EXISTS `credit_note_item`;
CREATE TABLE `credit_note_item` (
  `credit_note_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_note_id` int(11) DEFAULT NULL,
  `credit_note_amount` double NOT NULL,
  `invoice_type_id` int(11) NOT NULL,
  `credit_note_item_status` int(11) NOT NULL DEFAULT '1',
  `credit_note_item_created` date NOT NULL,
  `credit_note_month` varchar(10) NOT NULL,
  `credit_note_year` int(11) NOT NULL,
  `lease_id` int(11) NOT NULL,
  `personnel_id` int(20) DEFAULT NULL,
  `invoice_id` int(50) DEFAULT NULL,
  PRIMARY KEY (`credit_note_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of credit_note_item
-- ----------------------------

-- ----------------------------
-- Table structure for `credit_notes`
-- ----------------------------
DROP TABLE IF EXISTS `credit_notes`;
CREATE TABLE `credit_notes` (
  `credit_note_id` int(30) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) DEFAULT NULL,
  `personnel_id` int(30) NOT NULL,
  `credit_note_amount` double NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `credit_note_created` date DEFAULT NULL,
  `credit_note_created_by` int(11) DEFAULT NULL,
  `credit_note_status` int(11) DEFAULT '1',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `date_approved` date DEFAULT NULL,
  `cancel` tinyint(1) DEFAULT '0',
  `cancel_action_id` int(11) DEFAULT NULL,
  `cancel_description` text,
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_date` datetime DEFAULT NULL,
  `receipt_number` varchar(100) DEFAULT NULL,
  `lease_id` int(11) DEFAULT NULL,
  `credit_note_date` date DEFAULT NULL,
  `year` varchar(11) DEFAULT NULL,
  `month` varchar(10) DEFAULT NULL,
  `rental_unit_id` int(11) NOT NULL,
  `document_number` int(50) DEFAULT NULL,
  `prefix` varchar(20) DEFAULT NULL,
  `suffix` int(50) DEFAULT NULL,
  `account_id` int(50) DEFAULT NULL,
  `memo` text,
  `remarks` text,
  `jvno` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `sync_status` int(11) DEFAULT '0',
  PRIMARY KEY (`credit_note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of credit_notes
-- ----------------------------

-- ----------------------------
-- Table structure for `debit_note_item`
-- ----------------------------
DROP TABLE IF EXISTS `debit_note_item`;
CREATE TABLE `debit_note_item` (
  `debit_note_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `debit_note_id` int(11) DEFAULT NULL,
  `debit_note_amount` double NOT NULL,
  `invoice_type_id` int(11) NOT NULL,
  `debit_note_item_status` int(11) NOT NULL DEFAULT '1',
  `debit_note_item_created` date NOT NULL,
  `debit_note_month` varchar(10) NOT NULL,
  `debit_note_year` int(11) NOT NULL,
  `lease_id` int(11) NOT NULL,
  `personnel_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`debit_note_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of debit_note_item
-- ----------------------------

-- ----------------------------
-- Table structure for `debit_notes`
-- ----------------------------
DROP TABLE IF EXISTS `debit_notes`;
CREATE TABLE `debit_notes` (
  `debit_note_id` int(30) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) DEFAULT NULL,
  `personnel_id` int(30) NOT NULL,
  `debit_note_amount` double NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `debit_note_created` date DEFAULT NULL,
  `debit_note_created_by` int(11) DEFAULT NULL,
  `debit_note_status` int(11) DEFAULT '1',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `date_approved` date DEFAULT NULL,
  `cancel` tinyint(1) DEFAULT '0',
  `cancel_action_id` int(11) DEFAULT NULL,
  `cancel_description` text,
  `cancelled_by` int(11) DEFAULT NULL,
  `cancelled_date` datetime DEFAULT NULL,
  `receipt_number` varchar(100) DEFAULT NULL,
  `lease_id` int(11) DEFAULT NULL,
  `debit_note_date` date DEFAULT NULL,
  `year` varchar(11) DEFAULT NULL,
  `month` varchar(10) DEFAULT NULL,
  `rental_unit_id` int(11) NOT NULL,
  `document_number` int(50) DEFAULT NULL,
  `prefix` varchar(20) DEFAULT NULL,
  `suffix` int(50) DEFAULT NULL,
  `account_id` int(50) DEFAULT NULL,
  `memo` text,
  `remarks` text,
  `jvno` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `sync_status` int(11) DEFAULT '0',
  PRIMARY KEY (`debit_note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of debit_notes
-- ----------------------------

-- ----------------------------
-- Table structure for `ticket`
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_number` int(11) DEFAULT NULL,
  `ticket_description` text,
  `tenant_id` int(11) NOT NULL,
  `lease_id` int(11) NOT NULL,
  `created` date DEFAULT NULL,
  `ticket_status_id` int(11) NOT NULL,
  `ticket_deleted` int(11) DEFAULT '0',
  `ticket_status` int(11) DEFAULT '1',
  `general_observations` text,
  `date_closed` datetime DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket
-- ----------------------------

-- ----------------------------
-- Table structure for `ticket_status`
-- ----------------------------
DROP TABLE IF EXISTS `ticket_status`;
CREATE TABLE `ticket_status` (
  `ticket_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_status_name` varchar(255) DEFAULT NULL,
  `created` date NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `description` text,
  PRIMARY KEY (`ticket_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_status
-- ----------------------------

-- ----------------------------
-- Table structure for `user_log`
-- ----------------------------
DROP TABLE IF EXISTS `user_log`;
CREATE TABLE `user_log` (
  `user_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `personnel_id` int(11) DEFAULT NULL,
  `logged_date` date DEFAULT NULL,
  `session_number` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_log
-- ----------------------------
