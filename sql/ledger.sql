CREATE OR REPLACE VIEW v_general_ledger AS
-- Creditr Invoices
SELECT
	`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
	`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
	`creditor_invoice`.`invoice_number` AS `referenceCode`,
	'' AS `transactionCode`,
	`creditor_invoice`.`property_id` AS `projectId`,
    `creditor_invoice`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_invoice_item`.`item_description` AS `transactionName`,
	`creditor_invoice_item`.`item_description` AS `tranctionDescription`,
	`creditor_invoice_item`.`total_amount` AS `dr_amount`,
	'0' AS `cr_amount`,
	`creditor_invoice`.`transaction_date` AS `transactionDate`,
	`creditor_invoice`.`created` AS `createdAt`,
	`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'Creditors Invoices' AS `transactionClassification`,
	'creditor_invoice_item' AS `transactionTable`,
	'creditor_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_invoice_item`
				JOIN `creditor_invoice` ON(
					(
						creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

  UNION ALL
-- credit invoice payments

  SELECT
	`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
	`creditor_payment`.`creditor_payment_id` AS `referenceId`,
	`creditor_payment`.`reference_number` AS `referenceCode`,
	`creditor_payment_item`.`creditor_invoice_id` AS `transactionCode`,
	'' AS `projectId`,
  '' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_payment_item`.`description` AS `transactionName`,
	`creditor_payment_item`.`description` AS `tranctionDescription`,
	0 AS `dr_amount`,
	`creditor_payment_item`.`amount_paid` AS `cr_amount`,
	`creditor_payment`.`transaction_date` AS `transactionDate`,
	`creditor_payment`.`created` AS `createdAt`,
	`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Invoices Payments' AS `transactionClassification`,
	'creditor_payment' AS `transactionTable`,
	'creditor_payment_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_payment_item`
				JOIN `creditor_payment` ON(
					(
						creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
	`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
	`creditor_credit_note`.`invoice_number` AS `referenceCode`,
	`creditor_credit_note_item`.`creditor_invoice_id` AS `transactionCode`,
	'' AS `projectId`,
  '' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_credit_note`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_credit_note_item`.`description` AS `transactionName`,
	`creditor_credit_note_item`.`description` AS `tranctionDescription`,
	0 AS `dr_amount`,
	`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
	`creditor_credit_note`.`transaction_date` AS `transactionDate`,
	`creditor_credit_note`.`created` AS `createdAt`,
	`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Credit Notes' AS `transactionClassification`,
	'creditor_credit_note' AS `transactionTable`,
	'creditor_credit_note_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_credit_note_item`
				JOIN `creditor_credit_note` ON(
					(
						creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_credit_note.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`finance_purchase`.`finance_purchase_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `referenceCode`,
	`finance_purchase`.`transaction_number` AS `transactionCode`,
	`finance_purchase`.`property_id` AS `projectId`,
	`finance_purchase`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	`finance_purchase`.`finance_purchase_description` AS `tranctionDescription`,
	`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase`.`finance_purchase_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase`
				JOIN account ON(
					(
						account.account_id = finance_purchase.account_to_id
					)
				)
			)

		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
	`finance_purchase`.`finance_purchase_id` AS `referenceId`,
	`finance_purchase`.`document_number` AS `referenceCode`,
	`finance_purchase`.`transaction_number` AS `transactionCode`,
	finance_purchase.property_id AS `projectId`,
  finance_purchase.creditor_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'' AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`) AS `tranctionDescription`,
	0 AS `dr_amount`,
	`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'finance_purchase_payment' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase_payment`
				JOIN `finance_purchase` ON(
					(
						finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = finance_purchase_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

  SELECT
  	`finance_transfer`.`finance_transfer_id` AS `transactionId`,
  	`finance_transfered`.`finance_transfered_id` AS `referenceId`,
  	'' AS `referenceCode`,
  	`finance_transfer`.`reference_number` AS `transactionCode`,
  	'' AS `projectId`,
    '' AS `recepientId`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfer`.`account_from_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfer`.`remarks` AS `transactionName`,
  	CONCAT(' Amount Transfered to ',(SELECT account_name FROM account WHERE account_id = finance_transfered.account_to_id )) AS `tranctionDescription`,
  	0 AS `dr_amount`,
  	`finance_transfer`.`finance_transfer_amount` AS `cr_amount`,
  	`finance_transfered`.`transaction_date` AS `transactionDate`,
  	`finance_transfered`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfered' AS `transactionTable`,
  	'finance_transfer' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`finance_transfer`
  				JOIN `finance_transfered` ON(
  					(
  						finance_transfered.finance_transfer_id = finance_transfer.finance_transfer_id
  					)
  				)
  			)
  			JOIN account ON(
  				(
  					account.account_id = finance_transfer.account_from_id
  				)
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)


  UNION ALL
  SELECT
  	`finance_transfered`.`finance_transfered_id` AS `transactionId`,
  	`finance_transfer`.`finance_transfer_id` AS `referenceId`,
  	`finance_transfer`.`reference_number` AS `referenceCode`,
  	'' AS `transactionCode`,
  	'' AS `projectId`,
    '' AS `recepientId`,
  	`account`.`parent_account` AS `accountParentId`,
  	`account_type`.`account_type_name` AS `accountsclassfication`,
  	`finance_transfered`.`account_to_id` AS `accountId`,
  	`account`.`account_name` AS `accountName`,
  	`finance_transfered`.`remarks` AS `transactionName`,
  	 CONCAT('Amount Received from ',(SELECT account_name FROM account WHERE account_id = finance_transfer.account_from_id )) AS `tranctionDescription`,
  	`finance_transfered`.`finance_transfered_amount` AS `dr_amount`,
     0 AS `cr_amount`,
  	`finance_transfer`.`transaction_date` AS `transactionDate`,
  	`finance_transfer`.`created` AS `createdAt`,
  	`finance_transfer`.`finance_transfer_status` AS `status`,
  	'Transfer' AS `transactionCategory`,
  	'Transfer' AS `transactionClassification`,
  	'finance_transfer' AS `transactionTable`,
  	'finance_transfered' AS `referenceTable`
  FROM
  	(
  		(
  			(
  				`finance_transfered`
  				JOIN `finance_transfer` ON(
  					(
  						finance_transfer.finance_transfer_id = finance_transfered.finance_transfer_id
  					)
  				)
  			)
  			JOIN account ON(
  				(
  					account.account_id = finance_transfered.account_to_id
  				)
  			)
  		)
  		JOIN `account_type` ON(
  			(
  				account_type.account_type_id = account.account_type_id
  			)
  		)
  	)

		UNION ALL

		SELECT
		`invoice`.`invoice_id` AS `transactionId`,
		`lease_invoice`.`lease_invoice_id` AS `referenceId`,
		`lease_invoice`.document_number AS `referenceCode`,
		`invoice`.invoice_type AS `transactionCode`,
		`property`.`property_id` AS `projectId`,
	    `lease_invoice`.`lease_id` AS `recepientId`,
		`account`.`parent_account` AS `accountParentId`,
		`account_type`.`account_type_name` AS `accountsclassfication`,
		`invoice_type`.`generaljournalaccountid` AS `accountId`,
		`account`.`account_name` AS `accountName`,
		`invoice_type`.invoice_type_name AS `transactionName`,
		CONCAT(invoice_type.invoice_type_name,' Charge for unit ',rental_unit.rental_unit_name,' -- ',property.property_name) AS `tranctionDescription`,
		0 AS `dr_amount`,
		`invoice`.`invoice_amount` AS `cr_amount`,
		`lease_invoice`.`invoice_date` AS `transactionDate`,
		`lease_invoice`.`created` AS `createdAt`,
		`invoice`.`invoice_item_status` AS `status`,
		'Revenue' AS `transactionCategory`,
		'Tenants Invoices' AS `transactionClassification`,
		'invoice' AS `transactionTable`,
		'lease_invoice' AS `referenceTable`
	FROM
		(
			(
				(
					`invoice`
					JOIN `lease_invoice` ON(
						(
							lease_invoice.lease_invoice_id = invoice.lease_invoice_id
						)
					)
				)
				JOIN invoice_type ON(
					(
						invoice.invoice_type = invoice_type.invoice_type_id
					)
				)
				JOIN account ON(
					(
						account.account_id = invoice_type.generaljournalaccountid
					)
				)
				JOIN rental_unit ON(
					(
						rental_unit.rental_unit_id = lease_invoice.rental_unit_id
					)
				)
				JOIN property ON(
					(
						property.property_id = rental_unit.property_id
					)
				)
			)
			JOIN `account_type` ON(
				(
					account_type.account_type_id = account.account_type_id
				)
			)
		)

		UNION ALL
		SELECT
			`payment_item`.`payment_item_id` AS `transactionId`,
			`payments`.`payment_id` AS `referenceId`,
			`payments`.document_number AS `referenceCode`,
			`payment_item`.invoice_id AS `transactionCode`,
			`property`.`property_id` AS `projectId`,
		  `payments`.`lease_id` AS `recepientId`,
			`account`.`parent_account` AS `accountParentId`,
			`account_type`.`account_type_name` AS `accountsclassfication`,
			`payments`.`bank_id` AS `accountId`,
			`account`.`account_name` AS `accountName`,
			`payments`.receipt_number AS `transactionName`,
			CONCAT(invoice_type.invoice_type_name,' Payment for unit ',rental_unit.rental_unit_name,' -- ',property.property_name,' ',payment_item.remarks) AS `tranctionDescription`,
			`payment_item`.`amount_paid` AS `dr_amount`,
			0 AS `cr_amount`,
			`payments`.`payment_date` AS `transactionDate`,
			`payments`.`payment_created` AS `createdAt`,
			`payment_item`.`payment_item_status` AS `status`,
			'Revenue Payment' AS `transactionCategory`,
			'Tenants Payments' AS `transactionClassification`,
			'payment_item' AS `transactionTable`,
			'payments' AS `referenceTable`
		FROM
			(
				(
					(
						`payment_item`
						JOIN `payments` ON(
							(
								payments.payment_id = payment_item.payment_id
							)
						)
					)
					JOIN invoice_type ON(
						(
							invoice_type.invoice_type_id = payment_item.invoice_type_id
						)
					)
					JOIN lease_invoice ON(
						(
							lease_invoice.lease_invoice_id = payment_item.invoice_id
						)
					)
					JOIN account ON(
						(
							account.account_id = payments.bank_id
						)
					)
					JOIN rental_unit ON(
						(
							rental_unit.rental_unit_id = payments.rental_unit_id
						)
					)
					JOIN property ON(
						(
							property.property_id = rental_unit.property_id
						)
					)
				)
				JOIN `account_type` ON(
					(
						account_type.account_type_id = account.account_type_id
					)
				)
			)

			UNION ALL

			SELECT
				`credit_note_item`.`credit_note_item_id` AS `transactionId`,
				`credit_notes`.`credit_note_id` AS `referenceId`,
				`credit_notes`.document_number AS `referenceCode`,
				`credit_note_item`.invoice_id AS `transactionCode`,
				`property`.`property_id` AS `projectId`,
			    `credit_notes`.`lease_id` AS `recepientId`,
				`account`.`parent_account` AS `accountParentId`,
				`account`.`account_name` AS `accountsclassfication`,
				`credit_notes`.`bank_id` AS `accountId`,
				`account`.`account_name` AS `accountName`,
				`credit_notes`.receipt_number AS `transactionName`,
				CONCAT(invoice_type.invoice_type_name,' Credit note for unit ',rental_unit.rental_unit_name,' -- ',property.property_name) AS `tranctionDescription`,
				0 AS `dr_amount`,
				`credit_note_item`.`credit_note_amount` AS `cr_amount`,
				`credit_notes`.`credit_note_date` AS `transactionDate`,
				`credit_notes`.`credit_note_created` AS `createdAt`,
				`credit_note_item`.`credit_note_item_status` AS `status`,
				'Expense' AS `transactionCategory`,
				'Tenants Credit Notes' AS `transactionClassification`,
				'credit_note_item' AS `transactionTable`,
				'credit_notes' AS `referenceTable`
			FROM
				(
					(
						(
							`credit_note_item`
							JOIN `credit_notes` ON(
								(
									credit_notes.credit_note_id = credit_note_item.credit_note_id
								)
							)
						)
						JOIN invoice_type ON(
							(
								invoice_type.invoice_type_id = credit_note_item.invoice_type_id
							)
						)
						JOIN lease_invoice ON(
							(
								lease_invoice.lease_invoice_id = credit_note_item.invoice_id
							)
						)
						JOIN account ON(
							(
								account.account_id = credit_notes.bank_id
							)
						)
						JOIN rental_unit ON(
							(
								rental_unit.rental_unit_id = credit_notes.rental_unit_id
							)
						)
						JOIN property ON(
							(
								property.property_id = rental_unit.property_id
							)
						)
					)
				)
				UNION ALL

				SELECT
					`invoice`.`invoice_id` AS `transactionId`,
					`lease_invoice`.`lease_invoice_id` AS `referenceId`,
					`lease_invoice`.document_number AS `referenceCode`,
					`invoice`.invoice_type AS `transactionCode`,
					`property`.`property_id` AS `projectId`,
					 `lease_invoice`.`lease_id` AS `recepientId`,
					(SELECT account_type_id FROM account WHERE account_name = "Tax") AS `accountParentId`,
					 'Tax' AS `accountsclassfication`,
					(SELECT account_id FROM account WHERE account_name = "Tax") AS `accountId`,
					'' AS `accountName`,
					'Tax' AS `transactionName`,
					CONCAT('Tax amount on ',invoice_type.invoice_type_name,'for unit ',rental_unit.rental_unit_name,' -- ',property.property_name) AS `tranctionDescription`,
					0 AS `dr_amount`,
					`invoice`.`tax_amount` AS `cr_amount`,
					`lease_invoice`.`invoice_date` AS `transactionDate`,
					`lease_invoice`.`created` AS `createdAt`,
					`invoice`.`invoice_item_status` AS `status`,
					'Tax' AS `transactionCategory`,
					'Bill Tax' AS `transactionClassification`,
					'invoice' AS `transactionTable`,
					'lease_invoice' AS `referenceTable`
				FROM
					(
						(
							(
								`invoice`
								JOIN `lease_invoice` ON(
									(
										lease_invoice.lease_invoice_id = invoice.lease_invoice_id
									)
								)
							)
							JOIN invoice_type ON(
								(
									invoice.invoice_type = invoice_type.invoice_type_id
								)
							)
							JOIN account ON(
								(
									account.account_id = invoice_type.generaljournalaccountid
								)
							)
							JOIN rental_unit ON(
								(
									rental_unit.rental_unit_id = lease_invoice.rental_unit_id
								)
							)
							JOIN property ON(
								(
									property.property_id = rental_unit.property_id
								)
							)
						)
						JOIN `account_type` ON(
							(
								account_type.account_type_id = account.account_type_id
							)
						)

					)
				WHERE tax_amount > 0;



CREATE OR REPLACE VIEW v_general_ledger_by_date AS select * from v_general_ledger ORDER BY createdAt;

CREATE OR REPLACE VIEW v_bank_account_balances AS SELECT accountId,accountName,SUM(dr_amount),SUM(cr_amount), (SUM(dr_amount) - SUM(cr_amount)) AS balance FROM v_general_ledger where accountParentId=1  GROUP BY accountId order by createdAt
